# The Cottonwood Machine Learning Framework

(Shortcut to [the cheatsheet](cheatsheet.md))

Cottonwood is built to be as flexible as possible, top to bottom.
It's designed to minimize the iteration time when running experiments
and testing ideas. It's meant to be tweaked. Fork it. Add to it. Customize it
to solve the problem at hand. For more of the thought behind it, read
the post "
[Why another framework?](https://end-to-end-machine-learning.teachable.com/blog/171633/cottonwood-flexible-neural-network-framework)
[But seriously, why?](https://end-to-end-machine-learning.teachable.com/blog/204347/cottonwood-now-lets-you-build-complex-models)
and
[Why did you name it that?](https://end-to-end-machine-learning.teachable.com/blog/193739/why-is-it-called-cottonwood)

This code is always evolving. I recommend referencing a specific tag
whenever you use it in a project. Tags are labeled v1, v2, etc. and
the code attached to each one won't change.

[Here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24071959)
and [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24080930)
is a two-part video tour of a recent Cottonwood version.

If you want to follow along with the construction process for Cottonwood,
you can get a step-by-step walkthrough in End-to-End Machine Learning
[Course 312](https://end-to-end-machine-learning.teachable.com/p/write-a-neural-network-framework/).
[Course 313](https://end-to-end-machine-learning.teachable.com/p/advanced-neural-network-methods/),
and
[Course 314](https://end-to-end-machine-learning.teachable.com/p/314-neural-network-optimization/).

## Installation

Whether you want to pull Cottonwood into another project, 
or experiment with ideas of your own, you'll want
to clone the repository to your local machine and install it from there.

```bash
git clone https://gitlab.com/brohrer/cottonwood.git
python3 -m pip install -e cottonwood
```

## Try it out

```bash
python3
```
```python3
>>> import cottonwood.test
```

Here is
[the cheatsheet for pulling the relevant components](cheatsheet.md)
into your work.

## Versioning

Cottonwood versions are **not** guaranteed backward compatible.
You can select a particular version to work from.

```
cd cottonwood
git checkout v28
```

## Examples

See what Cottonwood looks like in action.
Feel free to use [any of these projects](https://e2eml.school/209)
as a template for a project of your own.
They're MIT licensed.
