## The bits you'll probably want to use

### [Structure](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/structure.py)
```python   
structure.Structure
    Structure.add()
    Structure.connect()
    Structure.connect_sequence()
    Structure.forward_pass()
    Structure.backward_pass()
    Structure.remove()
    Structure.save_structure()
    Structure.load_structure()
```

### [Neural Network Layers](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/)
```python   
dropout.Dropout
linear.Linear
normalization.Bias
normalization.MinMaxNormalization
experimental.online_normalization.OnlineNormalization
experimental.online_normalization_1d.OnlineNormalization_1D
experimental.online_normalization_2d.OnlineNormalization_2D
normalization.Scale
experimental.sparsify.Sparsify

conv1d.Conv1D
conv2d.Conv2D
pooling.AvgPool1D
pooling.AvgPool2D
pooling.MaxPool1D
pooling.MaxPool2D
experimental.online_normalization_1d.OnlineNormalization1D
experimental.online_normalization_2d.OnlineNormalization2D
```

### [Activation functions](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/activation.py)
```python   
activation.Logistic
activation.Sigmoid (alias for Logistic)
activation.ReLU
activation.SoftMax
activation.Tanh
```

### [Loss functions](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/loss.py)
```python
loss.AbsoluteLoss
loss.CrossEntropy
loss.Hinge
loss.SquareLoss
```

### [Initializers](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/initialization.py)
```python
initialization.LSUV
initialization.Glorot
initialization.He
experimental.initialization.Uniform
```

### Algorithms and methods
```python
knn.KNN
```

### [Operations](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/operations.py)
```python
operations.Constant
operations.Copy
operations.Difference
operations.Flatten
operations.HardMax
operations.NormalSample
operations.OneHot
operations.Stack
```

### [2D Operations](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/operations_2d.py)
```python
operations_2d.Crop
operations_2d.CropUniform
operations_2d.Pad
operations_2d.Resample
operations_2d.Stack
operations_2d.SquarePad
operations_2d.UpsampleDouble
```

### [Optimizers](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/optimization.py)
```python
optimization.Adam
optimization.Momentum
optimization.SGD
```

### Tools
```python
logging.ConfusionLogger
logging.DistributionLogger
logging.ValueLogger
toolbox.summarize()
experimental.visualize_structure
experimental.visualize_conv1d
experimental.visualize_conv2d_block
experimental.visualize_conv2d_kernels
cottonwood_test.test (https://gitlab.com/brohrer/cottonwood-test)
```

### [Data Blocks](https://gitlab.com/brohrer/cottonwood/blob/main/cottonwood/data/)
```python
data.two_by_two
data.three_by_three
data.nordic_runes
data.blips
cottonwood_data_diamonds.diamonds_block (https://gitlab.com/brohrer/cottonwood-data-diamonds/)
cottonwood_data_mnist.mnist_block (https://gitlab.com/brohrer/cottonwood-data-mnist/)
```
