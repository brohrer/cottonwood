import numpy as np


class Chop:
    def __init__(self, limits=1):
        self.limits = limits
        self.i_large = None
        self.i_small = None
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "chopped linear activation"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = self.forward_in.copy()
        self.i_large = np.where(self.forward_out > self.limits)
        self.i_small = np.where(self.forward_out < -self.limits)
        self.forward_out[self.i_large] = self.limits
        self.forward_out[self.i_small] = -self.limits
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        self.backward_out = self.backward_in.copy()
        self.backward_out[self.i_large] = 0
        self.backward_out[self.i_small] = 0
        return self.backward_out


class MirroredLog:
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "mirrored natural log activation"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = (
            np.sign(self.forward_in) * np.log(np.abs(self.forward_in) + 1))
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        self.backward_out = (
            np.sign(self.backward_in) / (np.abs(self.backward_in) + 1))
        return self.backward_out


class MirroredSqrt:
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None
        self.epsilon = 1e-6

    def __str__(self):
        return "mirrored square root activation"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = (
            np.sign(self.forward_in) * np.sqrt(np.abs(self.forward_in)))
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        gradient_magnitude = np.abs(self.backward_in)

        # Make sure the derivative is well defined at zero
        gradient_magnitude[
            np.where(gradient_magnitude < self.epsilon)] = self.epsilon

        self.backward_out = (
            np.sign(self.backward_in) / (gradient_magnitude ** (-.5)))
        return self.backward_out
