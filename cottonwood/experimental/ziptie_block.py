import numpy as np
from ziptie.algo import Ziptie as ZiptieAlgo


class Ziptie:
    """
    A Cottonwood block wrapping the ziptie algorithm as implemented in
    https://gitlab.com/brohrer/ziptie
    """
    def __init__(self, n_outputs=None, threshold=1e3):
        self.algo = None
        self.threshold = threshold
        self.n_inputs = None
        self.n_outputs = int(n_outputs)
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "Ziptie channel clustering algorithm"
            f"number of input channels: {self.algo.n_cables}",
            f"agglomeration threshold: {self.algo.agglomeration_threshold}",
            f"nucleation threshold: {self.algo.nucleation_threshold}",
        ]
        return "\n".join(str_parts)

    def is_full(self):
        if self.algo is None:
            return False
        return self.algo.n_bundles >= self.n_outputs

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.forward_in is None:
            self.forward_out is None
            return self.forward_out

        if self.algo is None:
            self.n_inputs = self.forward_in.size
            self.algo = ZiptieAlgo(
                n_cables=self.n_inputs,
                threshold=self.threshold)
            if self.n_outputs is None:
                self.n_outputs = self.n_inputs

        if self.algo.n_bundles < self.n_outputs:
            self.algo.create_new_bundles()
            self.algo.grow_bundles()
        bundle_activities = self.algo.update_bundles(self.forward_in)

        # Only pass forward results if the set of features is full.
        # This is useful for sequential training of successive zipties.
        if not self.is_full():
            self.forward_out is None
            return self.forward_out

        # Pad or truncate bundle_activities to be the right size
        self.forward_out = np.zeros(self.n_outputs)
        n_bundle_activities_keep = np.minimum(
            bundle_activities.size, self.n_outputs)
        self.forward_out[:n_bundle_activities_keep] = (
            bundle_activities[:n_bundle_activities_keep])
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in

        if self.backward_in is None:
            self.backward_out = None
        else:
            # Pad or truncate bundle_activities to be the right size
            bundle_activities = np.zeros(self.algo.n_bundles)
            n_bundle_activities_keep = np.minimum(
                bundle_activities.size, self.backward_in.size)
            bundle_activities[:n_bundle_activities_keep] = (
                self.backward_in[:n_bundle_activities_keep])
            self.backward_out = self.algo.project_bundle_activities(
                bundle_activities)
        return self.backward_out
