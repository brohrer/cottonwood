import time


class Heartbeat:
    """
    Enforce a minimum interval between forward passes
    """
    def __init__(self, interval_ms=100):
        """
        interval is how many milliseconds need to be between iterations
        """
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None
        self.interval = interval_ms
        self.last_timestamp = time.time()

    def __str__(self):
        str_parts = [
            "hearbeat",
            f"interval: {self.interval}"
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = self.forward_in

        current_timestamp = time.time()
        remaining = self.interval / 1000 - (
            current_timestamp - self.last_timestamp)
        if remaining > 0:
            time.sleep(remaining)
        else:
            duration = (current_timestamp - self.last_timestamp) * 1000
            print("Heads up: The heartbeat block didn't limit")
            print(f"this iteration' duration to {self.interval} ms because")
            print(f"it took longer than that ({duration} ms) to run.")

        self.last_timestamp = time.time()

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        self.backward_out = self.backward_in
        return self.backward_out
