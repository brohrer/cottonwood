import numpy as np


class SquareDeadzone:
    """
    This block calculates a loss value as the sum of the square of its inputs.
    It also provides a derivative of the loss with respect to each input
    on the backward pass.

    If connected to two input nodes, it will find the sum of the square
    of the difference ot the two nodes' values.

    It incorporates a deazone around zero.
    """
    def __init__(self):
        self.single_arg = False
        self.deadzone = .2

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "squared error loss with deadzone"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if type(self.forward_in) == tuple:
            self.single_arg = False
        else:
            self.single_arg = True

        if self.single_arg:
            x1 = self.forward_in
            deadzone_difference = np.maximum(
                0, np.abs(x1) - self.deadzone)
            self.forward_out = np.sum(deadzone_difference ** 2)
        else:
            x1, x2 = self.forward_in
            difference = np.abs(x1 - x2)
            deadzone_difference = np.maximum(0, difference - self.deadzone)
            self.forward_out = np.sum(deadzone_difference ** 2)

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.single_arg:
            self.backward_out = np.zeros(self.forward_in.shape)
            i_grad = np.where(np.abs(self.forward_in) > self.deadzone)
            self.backward_out[i_grad] = (
                2 * (np.abs(self.forward_in) - self.deadzone) *
                np.sign(self.forward_in)
            )[i_grad]
            return self.backward_out

        else:
            x1, x2 = self.forward_in
            difference = x1 - x2
            grad1 = np.zeros(difference.shape)
            i_grad = np.where(np.abs(difference) > self.deadzone)
            grad1[i_grad] = (
                2 * (np.abs(difference) - self.deadzone) * np.sign(difference)
            )[i_grad]
            self.backward_out = (grad1, -grad1)
            return self.backward_out


class SquareLossScaledTarget:
    """
    This block calculates a loss value as the sum of the square of its inputs.
    It also provides a derivative of the loss with respect to each input
    on the backward pass.

    If connected to two input nodes, it will find the sum of the square
    of the difference ot the two nodes' values.
    """
    def __init__(self):
        self.scale = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "squared error loss"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        x1, x2 = self.forward_in
        # Create a scale array
        # The element corresponding to the target category will be 1
        # All the others will be 1 / (n - 1), so as to balance the effect
        # of all the non-target categories.
        i_label = np.argmax(x2)
        self.scale = np.ones(x2.size) / (x2.size - 1)
        self.scale[i_label] = 1
        self.forward_out = np.sum(self.scale * (x1 - x2) ** 2)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        x1, x2 = self.forward_in

        grad1 = 2 * self.scale * (x1 - x2)
        self.backward_out = (grad1, -grad1)
        return self.backward_out
