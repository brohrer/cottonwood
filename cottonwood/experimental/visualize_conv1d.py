"""
Show the current state of a one dimensional convolution block.

parameter dictionary
All are floats, in units of centimeters
----------
dy_figure: figure height
dx_figure: figure width
dx_dy_figure_border: figure border width/height
dx_dy_frame_border: frame border width/height
dx_dy_frame_spacing: within frame spacing width/height
dy_text: height of text
n_frame_cols: number of columns of frames
n_frame_rows: number of rows of frames
dx_frame: frame width
dy_frame: frame height
dx_array_element_spacing: width of default spacing between points
dy_lolli_plots: height of the signal plots
dx_lolli_plot_area: width of the signal plots
"""
from copy import deepcopy
import os
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend("agg")


# Valid Matplotlib colors. More examples here:
# https://e2eml.school/matplotlib_lines.html#color
canvas_color = "#fbf9f6"  # warm white
kernel_color = "#050560"
signal_color = "mediumblue"
negative_color = "crimson"
border_color = "darkslateblue"
stem_color = "#e0e0d0"  # light gray
text_color = "#04253a"


def render(block, results_dir="results", filename="conv_kernels.png"):
    """
    This function is the entry point.

    block (Conv1D block object)
    path (str) is where the image should be stored
    filename (str) is the stem of the filename to use
    """
    # Find the number and layout of frames.
    # The plot area will be chopped up into n_rows x n_cols
    # separate frames.
    # The last column will be reserved for biases.
    # Start with an approximate aspect ratio (width / height) and
    # make sure the kernels fit in the first n_cols - 1 columns.
    approx_aspect_ratio = 2 / block.n_channels
    n_rows = int(np.ceil(np.sqrt(block.n_kernels * approx_aspect_ratio)))
    n_cols = int(np.ceil(block.n_kernels / n_rows))

    # Image resolution, dots per inch
    dpi = 600

    # Ensure that the necessary directories exist
    os.makedirs(results_dir, exist_ok=True)

    weights = deepcopy(block.weights)
    x = deepcopy(block.forward_in)

    # Scale all the values to a max magnitude of 1
    epsilon = 1e-6
    weights /= np.max(np.abs(weights) + epsilon)
    x /= np.max(np.abs(x) + epsilon)

    # Use the convolution results to control the transparency of
    # the plotted points.
    alphas = np.pad(
        deepcopy(block.forward_out),
        ((block.kernel_half, block.kernel_half), (0, 0))
    )
    alphas /= np.max(np.abs(alphas))

    # Calculate the parameters
    # Overall figure size
    dx_figure = 16
    dy_figure = 9

    # A little whitespace around the edges
    dx_dy_figure_border = .4

    # Some room for math symbols between plots
    dy_text = .3

    # The size of each frame
    dx_frame = (dx_figure - 2 * dx_dy_figure_border) / n_cols
    dy_frame = (dy_figure - dy_text - 2 * dx_dy_figure_border) / n_rows
    dx_dy_frame_border = np.minimum(dx_frame, dy_frame) * .1
    dx_dy_frame_spacing = np.minimum(
        dy_frame * .2 / (block.n_channels - 1 + 1e-6),
        dy_frame * .05
    )

    # Calculate the spacing between points
    # The minimum spacing allowed between points
    dx_array_element_spacing = (
        dx_frame
        - dx_dy_frame_border * 2
        - dx_dy_frame_spacing
    ) / (block.n_inputs + block.kernel_size)

    # Calculate the size of the plots
    dy_lolli_plots = (
        dy_frame
        - dx_dy_frame_border * 2
        - dx_dy_frame_spacing * (block.n_channels - 1)
    ) / block.n_channels
    dx_kernel_lolli_plot = dx_array_element_spacing * block.kernel_size
    dx_input_lolli_plot = dx_array_element_spacing * block.n_inputs

    figure, figure_axes = get_empty_frame(
        dx_figure=dx_figure,
        dy_figure=dy_figure,
        canvas_color=canvas_color,
        border_color=border_color,
    )

    # Draw frame borders
    border_linewidth = .5
    border_alpha = 1
    # Plot bottom border
    figure_axes.plot(
        [dx_dy_figure_border, dx_figure - dx_dy_figure_border],
        [dx_dy_figure_border + dy_text, dx_dy_figure_border + dy_text],
        color=border_color,
        linewidth=border_linewidth,
        alpha=border_alpha,
    )
    # Plot top border
    figure_axes.plot(
        [dx_dy_figure_border, dx_figure - dx_dy_figure_border],
        [dy_figure - dx_dy_figure_border, dy_figure - dx_dy_figure_border],
        color=border_color,
        linewidth=border_linewidth,
        alpha=border_alpha,
    )
    # Plot left border
    figure_axes.plot(
        [dx_dy_figure_border, dx_dy_figure_border],
        [dx_dy_figure_border + dy_text, dy_figure - dx_dy_figure_border],
        color=border_color,
        linewidth=border_linewidth,
        alpha=border_alpha,
    )
    # Plot right border
    figure_axes.plot(
        [dx_figure - dx_dy_figure_border, dx_figure - dx_dy_figure_border],
        [dx_dy_figure_border + dy_text, dy_figure - dx_dy_figure_border],
        color=border_color,
        linewidth=border_linewidth,
        alpha=border_alpha,
    )

    # Add text labels
    y_label_center = dx_dy_figure_border + dy_text / 2
    x_conv_label_center = (
        dx_dy_figure_border
        + n_cols * dx_frame / 2
    )
    figure_axes.text(
        x_conv_label_center,
        y_label_center,
        "kernels and convolution heatmaps",
        fontsize=6,
        color=text_color,
        horizontalalignment="center",
        verticalalignment="center",
    )

    # Draw the border grid
    x_frame_max = dx_figure - dx_dy_figure_border
    for i_row in range(n_rows):
        y_frame_border = (
            (i_row + 1) * dy_frame
            + dx_dy_figure_border
            + dy_text
        )
        figure_axes.plot(
            [dx_dy_figure_border, x_frame_max],
            [y_frame_border, y_frame_border],
            color=border_color,
            linewidth=border_linewidth,
            alpha=border_alpha,
        )
    for i_col in range(n_cols):
        x_frame_border = (i_col + 1) * dx_frame + dx_dy_figure_border
        figure_axes.plot(
            [x_frame_border, x_frame_border],
            [dx_dy_figure_border + dy_text, dy_figure - dx_dy_figure_border],
            color=border_color,
            linewidth=border_linewidth,
            alpha=border_alpha,
         )

    # Fill in the plots for each kernel
    for i_kernel in range(block.n_kernels):
        i_col = i_kernel % n_cols
        i_row = i_kernel // n_cols

        x_frame_left = dx_dy_figure_border + i_col * dx_frame
        y_frame_top = (
            dx_dy_figure_border
            + dy_text
            + (n_rows - i_row) * dy_frame
        )
        # Add plots for each channel
        for i_channel in range(block.n_channels):
            y_channel_axis = (
                y_frame_top
                - (i_channel + .5) * dy_lolli_plots
                - dx_dy_frame_border
                - i_channel * dx_dy_frame_spacing
            )
            x_kernel_plot_center = (
                x_frame_left
                + dx_dy_frame_border
                + dx_kernel_lolli_plot / 2
            )
            x_input_plot_center = (
                x_frame_left
                + dx_frame
                - dx_dy_frame_border
                - dx_input_lolli_plot / 2
            )
            # Add the kernel plot
            plot_lolli(
                figure_axes,
                weights[:, i_channel, i_kernel],
                x_kernel_plot_center,
                y_channel_axis,
                dx_array_element_spacing,
                dy_lolli_plots,
                color=kernel_color,
            )
            # Add the signal plot, modulated by the result
            # to create a heatmap
            plot_lolli(
                figure_axes,
                x[:, i_channel],
                x_input_plot_center,
                y_channel_axis,
                dx_array_element_spacing,
                dy_lolli_plots,
                color=signal_color,
                negative_color=negative_color,
                alpha_values=alphas[:, i_kernel],
            )
    figure.savefig(os.path.join(results_dir, filename), dpi=dpi)
    plt.close()


def get_empty_frame(
    dx_figure=16,  # centimeters
    dy_figure=9,  # centimeters
    canvas_color="white",
    border_color="black",
):
    """
    Generate a blank canvas on which to draw
    """
    # figsize expects inches. Convert from centimeters.
    figure = plt.figure(figsize=(dx_figure / 2.54, dy_figure / 2.54))

    # Cover the entire figure with an Axes object
    figure_axes = figure.add_axes((0, 0, 1, 1))

    # Ensure that 1 unit in the Axes is 1 centimeter in the final image
    figure_axes.set_xlim(0, dx_figure)
    figure_axes.set_ylim(0, dy_figure)

    figure_axes.set_facecolor(canvas_color)

    # Clean up the axes
    figure_axes.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    figure_axes.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    figure_axes.spines["top"].set_color(border_color)
    figure_axes.spines["bottom"].set_color(border_color)
    figure_axes.spines["left"].set_color(border_color)
    figure_axes.spines["right"].set_color(border_color)
    figure_axes.spines["top"].set_linewidth(4)
    figure_axes.spines["bottom"].set_linewidth(4)
    figure_axes.spines["left"].set_linewidth(4)
    figure_axes.spines["right"].set_linewidth(4)

    return figure, figure_axes


def plot_lolli(
    figure_axes,
    array,
    x_lolli_plot_center,
    y_lolli_plot_axis,
    dx_array_element_spacing,
    dy_lolli_plots,
    alpha_values=None,
    color="black",
    negative_color="darkred",
):
    """
    Create a lollipop-style plot of a one-dimensional array
    """
    dx_lolli_plot = array.size * dx_array_element_spacing
    x_lolli_plot_left = x_lolli_plot_center - dx_lolli_plot / 2
    x_lolli_plot_right = x_lolli_plot_center + dx_lolli_plot / 2

    # Scale the array to fill the allotted vertical space.
    # Assumes that the array has a maximum magnitude of 1
    array = array.copy()
    array *= dy_lolli_plots / 2
    if alpha_values is None:
        alpha_values = np.ones(array.size)
        sign_values = np.ones(array.size)
    else:
        sign_values = np.sign(alpha_values)
        alpha_values = np.abs(alpha_values)

    figure_axes.plot(
        [x_lolli_plot_left, x_lolli_plot_right],
        [y_lolli_plot_axis, y_lolli_plot_axis],
        color=stem_color,
        linewidth=.5,
    )

    dx_element = dx_array_element_spacing * .5
    element_linewidth = .5
    for i_element, element_value in enumerate(array):
        # Plot the stem
        x_element = (
            x_lolli_plot_left
            + (i_element + .5) * dx_array_element_spacing
        )
        y_element = y_lolli_plot_axis + element_value
        figure_axes.plot(
            [x_element, x_element],
            [y_lolli_plot_axis, y_element],
            color=stem_color,
            linewidth=1,
            solid_capstyle="butt",
            alpha=alpha_values[i_element],
        )
        # Plot a short bar to show the value
        if sign_values[i_element] > 0:
            figure_axes.plot(
                [x_element - dx_element, x_element + dx_element],
                [y_element, y_element],
                color=color,
                linewidth=element_linewidth,
                solid_capstyle="butt",
                zorder=5,
                alpha=alpha_values[i_element],
            )
        else:
            # If the value is negative, show it in a different color
            figure_axes.plot(
                [x_element - dx_element, x_element + dx_element],
                [y_element, y_element],
                color=negative_color,
                linewidth=element_linewidth,
                solid_capstyle="butt",
                zorder=5,
                alpha=alpha_values[i_element],
            )
