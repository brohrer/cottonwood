import numpy as np


class TranslationViews:
    """
    This block generates translated versions of 2D arrays.
    It's useful for enriching image data sets.
    """
    def __init__(self, max_pixel_shift=1):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        # The maximum number of pixels by which to translate the image.
        # This should be small compared to the image size.
        self.max_pixel_shift = int(max_pixel_shift)

    def __str__(self):
        return("Translated views")

    def forward_pass(self, forward_in):
        """
        Generate a set of translated views, expanded from a
        list of inital views.

        forward_in is expected to be either a 2D NumPy array of floats
            or a list of such arrays. If it's a list the first one is
            the canonical view.

        forward_out will be a list of 2D NumPy arrays. The first one will
            be identical to the canonical view, minus the border of
            max_pixel_shift.
        """
        self.forward_in = forward_in
        if type(self.forward_in) != list:
            self.forward_in = [self.forward_in]
        input_shape = self.forward_in[0].shape
        output_shape = (
            input_shape[0] - 2 * self.max_pixel_shift,
            input_shape[1] - 2 * self.max_pixel_shift)
        if np.min(output_shape) <= 1:
            raise ValueError(
                f"Image shape {input_shape} too small for translation " +
                f"of {self.max_pixel_shift}.")

        # Initialize a list of views, translated versions of the original
        self.forward_out = []
        translations  = (
            [0] +
            list(range(1, self.max_pixel_shift + 1)) +
            list(range(-self.max_pixel_shift, 0)))
        for source_view in self.forward_in:
            for row_translation in translations:
                for col_translation in translations:
                    row_offset = row_translation + self.max_pixel_shift
                    col_offset = col_translation + self.max_pixel_shift
                    translated = source_view[
                        row_offset: row_offset + output_shape[0],
                        col_offset: col_offset + output_shape[1]]
                    self.forward_out.append(translated)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out
