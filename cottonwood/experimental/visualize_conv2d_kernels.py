from copy import deepcopy
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from cottonwood.conv2d import convolve_2d
plt.switch_backend("agg")

# These are in centimeters
figure_wid = 16
figure_hgt = 9
border_wid = 1
border_hgt = 1
main_image_wid = 3
main_image_hgt = 3
n_col_factor = 3

# All gap, kernel, image, and unit dimensions are multiples of image_wid
gap_wid_unscaled = .15
gap_hgt_unscaled = .15
row_gap_hgt_unscaled = 1
kernel_wid_unscaled = .5
kernel_hgt_unscaled = .5
image_wid_unscaled = 1
image_hgt_unscaled = 1
unit_hgt_unscaled = image_hgt_unscaled + gap_hgt_unscaled + kernel_hgt_unscaled

# Valid Matplotlib colors. More examples here:
# https://e2eml.school/matplotlib_lines.html#color
canvas_color = "#060820"  # very dark blue
activity_cmap = LinearSegmentedColormap.from_list(
    "pos_neg",
    [(0, "red"), (.5, "black"), (1, "white")])
original_2d_cmap = "gray"
border_color = "darkslateblue"
border_color = "#fbf9f6"  # warm white
text_color = "#04253a"
border_linewidth = 1
axes_border_linewidth = .3


def render_kernel(
    block,
    i_kernel,
    original_image,
    reports_dir,
    filename,
):
    n_cols = int(np.sqrt(block.n_channels * n_col_factor))
    n_rows = int(np.ceil(block.n_channels / n_cols))

    # Image resolution, dots per inch
    dpi = 300

    kernel = deepcopy(block.weights[:, :, :, i_kernel])
    input_images = deepcopy(block.forward_in)
    output_channel = np.tanh(deepcopy(block.forward_out[:, :, i_kernel]))

    figure, figure_axes = get_empty_frame()

    # Add the kernel's output channel
    left = border_wid
    bottom = border_hgt
    ax_output_channel = add_empty_axes(
        figure, left, bottom, main_image_wid, main_image_hgt)
    ax_output_channel.imshow(
        output_channel,
        vmin=-1,
        vmax=1,
        cmap=activity_cmap)

    # Add the original image
    bottom = figure_hgt - main_image_hgt - border_hgt
    ax_original_image = add_empty_axes(
        figure, left, bottom, main_image_wid, main_image_hgt)
    if original_image is not None:
        if len(original_image.shape) == 2:
            ax_original_image.imshow(original_image, cmap=original_2d_cmap)
        else:
            ax_original_image.imshow(original_image)

    # How much area is available for individual channels
    channel_region_wid = figure_wid - 3 * border_wid - main_image_wid
    channel_region_hgt = figure_hgt - 2 * border_hgt

    # What would the printed area need to be scaled by
    # if constrained by figure height? width?
    area_wid_unscaled = (
        n_cols * image_wid_unscaled + (n_cols - 1) * gap_wid_unscaled)
    area_hgt_unscaled = (
        n_rows * unit_hgt_unscaled + (n_rows - 1) * row_gap_hgt_unscaled)
    wid_scale = channel_region_wid / area_wid_unscaled
    hgt_scale = channel_region_hgt / area_hgt_unscaled
    # Choose the most constraining dimension
    scale = np.minimum(wid_scale, hgt_scale)

    area_wid = area_wid_unscaled * scale
    area_hgt = area_hgt_unscaled * scale
    image_wid = image_wid_unscaled * scale
    image_hgt = image_hgt_unscaled * scale
    gap_wid = gap_wid_unscaled * scale
    gap_hgt = gap_hgt_unscaled * scale
    row_gap_hgt = row_gap_hgt_unscaled * scale
    kernel_wid = kernel_wid_unscaled * scale
    kernel_hgt = kernel_hgt_unscaled * scale
    unit_hgt = unit_hgt_unscaled * scale
    area_left = (
        main_image_wid +
        2 * border_wid +
        channel_region_wid / 2 -
        area_wid / 2)
    area_bottom = figure_hgt / 2 - area_hgt / 2

    # Fill in the plots for each channel
    i_channel = 0
    for i_col in range(n_cols):
        x_unit = area_left + i_col * (image_wid + gap_wid) + image_wid / 2
        for i_row in range(n_rows):
            y_unit = area_bottom + i_row * (unit_hgt + row_gap_hgt)
            image_ax = add_empty_axes(
                figure,
                x_unit - image_wid / 2,
                y_unit,
                image_wid,
                image_hgt)
            kernel_ax = add_empty_axes(
                figure,
                x_unit - kernel_wid / 2,
                y_unit + image_hgt + gap_hgt,
                kernel_wid,
                kernel_hgt)

            kernel_channel = kernel[:, :, i_channel]
            input_image = input_images[:, :, i_channel]
            activities = convolve_2d(input_image, kernel_channel)

            image_ax.imshow(
                np.tanh(activities),
                vmin=-1,
                vmax=1,
                cmap=activity_cmap)
            kernel_ax.imshow(np.tanh(kernel_channel), cmap=activity_cmap)

            i_channel += 1
            if i_channel >= block.n_channels:
                break

    figure.savefig(os.path.join(reports_dir, filename), dpi=dpi)
    plt.close()


def get_empty_frame():
    # figsize expects inches. Convert from centimeters.
    figure = plt.figure(figsize=(figure_wid / 2.54, figure_hgt / 2.54))

    # Cover the entire figure with an Axes object
    figure_axes = figure.add_axes((0, 0, 1, 1))

    # Ensure that 1 unit in the Axes is 1 centimeter in the final image
    figure_axes.set_xlim(0, figure_wid)
    figure_axes.set_ylim(0, figure_hgt)
    figure_axes.set_facecolor(canvas_color)

    # Clean up the axes
    figure_axes.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    figure_axes.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    figure_axes.spines["top"].set_color(border_color)
    figure_axes.spines["bottom"].set_color(border_color)
    figure_axes.spines["left"].set_color(border_color)
    figure_axes.spines["right"].set_color(border_color)
    figure_axes.spines["top"].set_linewidth(border_linewidth)
    figure_axes.spines["bottom"].set_linewidth(border_linewidth)
    figure_axes.spines["left"].set_linewidth(border_linewidth)
    figure_axes.spines["right"].set_linewidth(border_linewidth)
    return figure, figure_axes


def add_empty_axes(figure, left, bottom, width, height):

    # Cover the entire figure with an Axes object
    axes = figure.add_axes((
        left / figure_wid,
        bottom / figure_hgt,
        width / figure_wid,
        height / figure_hgt))

    # Clean up the axes
    axes.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    axes.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    axes.spines["top"].set_color(border_color)
    axes.spines["bottom"].set_color(border_color)
    axes.spines["left"].set_color(border_color)
    axes.spines["right"].set_color(border_color)
    axes.spines["top"].set_linewidth(axes_border_linewidth)
    axes.spines["bottom"].set_linewidth(axes_border_linewidth)
    axes.spines["left"].set_linewidth(axes_border_linewidth)
    axes.spines["right"].set_linewidth(axes_border_linewidth)

    return axes
