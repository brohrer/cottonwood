import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from cottonwood.structure import Structure

background_color = "ivory"


def render(
    structure,
    reports_dir="reports",
    viz_filename="structure_diagram.png",
):
    """
    Create an image showing all the blocks in the structure and
    how they are connected.
    """
    positions = find_node_positions(structure)
    generate_plot(structure, positions, reports_dir, viz_filename)


def generate_plot(structure, positions, reports_dir, viz_filename):
    """
    The mechanics of putting the plot together
    """
    for block, pos in positions.items():
        # If this particular block is also a structure, visualize it too
        if isinstance(structure.blocks[block], Structure):
            render(
                structure.blocks[block],
                reports_dir=reports_dir,
                viz_filename=block + "_" + viz_filename)

    os.makedirs(reports_dir, exist_ok=True)

    fig = plt.figure(figsize=(16/2.54, 9/2.54))
    ax = fig.add_axes((0, 0, 1, 1))
    ax.set_facecolor(background_color)
    ax.set_xlim(0, 16)
    ax.set_ylim(-4.5, 4.5)
    box_width = .4
    box_height = 1.8

    for block, pos in positions.items():

        # Add the label for each of the blocks
        ax.text(
            pos["x"], pos["y"],
            block,
            fontsize=6,
            horizontalalignment="center",
            verticalalignment="center",
            rotation=90,
            zorder=6,
        )
        # Add a rectangular patch around each label
        codes = [Path.MOVETO] + [Path.LINETO] * 3 + [Path.CLOSEPOLY]
        vertices = [
            (pos["x"] + box_width / 2, pos["y"] + box_height / 2),
            (pos["x"] - box_width / 2, pos["y"] + box_height / 2),
            (pos["x"] - box_width / 2, pos["y"] - box_height / 2),
            (pos["x"] + box_width / 2, pos["y"] - box_height / 2),
            (pos["x"] + box_width / 2, pos["y"] + box_height / 2),
        ]
        vertices = np.array(vertices, float)
        path = Path(vertices, codes)
        pathpatch = PathPatch(
            path,
            facecolor='white',
            edgecolor='black',
            zorder=0,
        )
        ax.add_patch(pathpatch)

        # Connect this block to each of the next blocks
        if structure.connections_by_tail.get(block) is not None:
            connections = structure.connections_by_tail[block]
            for i_port, connection in connections.items():
                x_tail = pos["x"]
                y_tail = pos["y"]

                head_block = connection["head_block"]
                x_head = positions[head_block]["x"]
                y_head = positions[head_block]["y"]
                plt.plot(
                    [pos["x"], x_head],
                    [pos["y"], y_head],
                    color="black",
                    zorder=-3,
                )

                # Add an arrowhead mid-connection to show direction
                s = .2  # Size of the arrowhead in cm
                x = (x_tail + x_head) / 2
                y = (y_tail + y_head) / 2
                t = np.arctan2(
                    y_head - y_tail,
                    x_head - x_tail)  # heading angle
                codes = [Path.MOVETO] + [Path.LINETO] * 2 + [Path.CLOSEPOLY]
                vertices = [
                    (x + s * np.cos(t), y + s * np.sin(t)),
                    (x + s * (-np.cos(t) - np.sin(t)) / 2,
                        y + s * (-np.sin(t) + np.cos(t)) / 2),
                    (x + s * (-np.cos(t) + np.sin(t)) / 2,
                        y + s * (-np.sin(t) - np.cos(t)) / 2),
                    (x + s * np.cos(t), y + s * np.sin(t)),
                ]
                vertices = np.array(vertices, float)
                path = Path(vertices, codes)
                pathpatch = PathPatch(
                    path,
                    facecolor='black',
                    edgecolor='black',
                    zorder=-1,
                )
                ax.add_patch(pathpatch)
    original_backend = plt.get_backend()
    plt.switch_backend("agg")
    fig.savefig(os.path.join(reports_dir, viz_filename), dpi=300)
    plt.switch_backend(original_backend)
    plt.show()
    plt.close()


def find_node_positions(structure):
    """
    Create a structure for the physics simulation and run it
    """
    sim = Structure()
    sim.add(PositionBlock(structure), "position")
    sim.add(ForceBlock(structure), "force")
    sim.connect("position", "force")

    while(sim.blocks["position"].done is False):
        sim.forward_pass()
        sim.backward_pass()
    return sim.blocks["position"].positions


class PositionBlock:
    """
    This block initializes and adjusts the positions of each of the blocks
    in the visualization.
    """
    def __init__(self, structure, height=9, width=16):
        self.structure = structure
        # Initialize position guesses for nodes
        self.width = width
        self.height = height
        self.x_left = 2
        self.x_right = self.width - self.x_left
        self.y_bottom = -3
        self.y_top = -self.y_bottom
        self.y_center = (self.y_bottom + self.y_top) / 2
        self.y_scale = (self.y_top - self.y_center) / 3

        self.structure.find_block_execution_order()
        all_blocks = self.structure.sorted_blocks

        self.starting_blocks = []
        non_starting_blocks = self.structure.connections_by_head.keys()
        for block_name in all_blocks:
            if block_name not in non_starting_blocks:
                self.starting_blocks.append(block_name)

        self.ending_blocks = []
        non_ending_blocks = self.structure.connections_by_tail.keys()
        for block_name in all_blocks:
            if block_name not in non_ending_blocks:
                self.ending_blocks.append(block_name)

        self.positions = {}
        x_all = np.linspace(self.x_left, self.x_right, len(all_blocks))
        for i_block, block in enumerate(all_blocks):
            x = x_all[i_block]
            y = np.random.normal(loc=self.y_center, scale=self.y_scale)
            if block in self.starting_blocks:
                x = self.x_left
            if block in self.ending_blocks:
                x = self.x_right
            self.positions[block] = {"x": x, "y": y}

        self.force_to_motion = 1e-3
        self.d_threshold = self.force_to_motion / 1e3
        self.d_max = self.width / 10
        self.i_iter = 0
        self.max_iter = 1e5
        self.done = False

    def __str__(self):
        return "node position adjustment"

    def forward_pass(self, arg):
        self.i_iter += 1
        return self.positions

    def backward_pass(self, forces):
        d_total = 0
        for block, force in forces.items():
            dx = forces[block]["x"] * self.force_to_motion
            if block in self.starting_blocks or block in self.ending_blocks:
                dx = 0
            dx = np.minimum(dx, self.d_max)
            dx = np.maximum(dx, -self.d_max)
            self.positions[block]["x"] += dx
            d_total += np.abs(dx)

            dy = forces[block]["y"] * self.force_to_motion
            dy = np.minimum(dy, self.d_max)
            dy = np.maximum(dy, -self.d_max)
            self.positions[block]["y"] += dy
            d_total += np.abs(dy)

        # for block, pos in self.positions.items():
        #     # Find all the immediate connections of the current block
        #     if self.structure.connections_by_head.get(block) is not None:
        #         connections = self.structure.connections_by_head[block]
        #         for i_port, connection in connections.items():
        #             x_tail = self.positions[connection["tail_block"]]["x"]
        #             if pos["x"] < x_tail:
        #                 pos["x"] = x_tail

        if d_total < self.d_threshold or self.i_iter > self.max_iter:
            self.done = True
        return None


class ForceBlock:
    """
    This block calculates forces based on the distance between
    each of the blocks in the visualization.
    If blocks get too close, they repel each other.
    Connected blocks pull toward each other.
    """
    def __init__(self, structure):
        self.structure = structure
        self.repulsion_magnitude = 2
        self.y_factor = 6
        self.attraction_magnitude = 4
        self.zero_y_attraction_magnitude = .001
        self.characteristic_length = None

        self.forces = {}
        for block in list(structure.blocks.keys()):
            self.forces[block] = {"x": 0, "y": 0}

    def __str__(self):
        return "node force calculation"

    def find_characteristic_length(self, positions):
        """
        Depending on the size of the image and the number of blocks being
        visualized, find the characteristic length over which the forces
        act.
        """
        x_min = 1e10
        x_max = -1e10
        y_min = 1e10
        y_max = -1e10
        n_blocks = 0
        for block, pos in positions.items():
            n_blocks += 1
            x_min = np.minimum(x_min, pos["x"])
            x_max = np.maximum(x_max, pos["x"])
            y_min = np.minimum(y_min, pos["y"])
            y_max = np.maximum(y_max, pos["y"])
        self.characteristic_length = np.maximum(
            (x_max - x_min), (y_max - y_min)) / n_blocks

    def forward_pass(self, positions):
        """
        Forces pull each node toward a good position for visualization
        An attractive force acts between connected nodes.
        A repulsive force acts between unconnected nodes.
        """
        if self.characteristic_length is None:
            self.find_characteristic_length(positions)

        for block_a, pos_a in positions.items():
            # Find all the immediate connections of the current block
            connected_blocks = []
            if self.structure.connections_by_head.get(block_a) is not None:
                connections = self.structure.connections_by_head[block_a]
                for i_port, connection in connections.items():
                    connected_blocks.append(connection["tail_block"])
            if self.structure.connections_by_tail.get(block_a) is not None:
                connections = self.structure.connections_by_tail[block_a]
                for i_port, connection in connections.items():
                    connected_blocks.append(connection["head_block"])

            self.forces[block_a]["x"] = 0
            self.forces[block_a]["y"] = 0

            for block_b, pos_b in positions.items():
                # A block can't interact with itself
                if block_a == block_b:
                    continue
                # The force between any two blocks is related to
                # the distance between them, in both the x- and y-directions.
                distance = (
                    (pos_a["x"] - pos_b["x"]) ** 2
                    + (pos_a["y"] - pos_b["y"]) ** 2) ** .5
                angle = np.arctan2(
                    pos_a["y"] - pos_b["y"],
                    pos_a["x"] - pos_b["x"])

                # If the blocks are connected,
                # The attractive force between the blocks scales
                # proportionally to the distance.
                if block_b in connected_blocks:
                    attraction = (
                        self.attraction_magnitude
                        * (distance / self.characteristic_length) ** 2)
                    self.forces[block_a]["x"] -= np.cos(angle) * attraction
                    self.forces[block_a]["y"] -= np.sin(angle) * attraction

                # If the blocks aren't connected,
                # the repulsive forces between blocks
                # are related to the inverse of the distance.
                repulsion = (
                    self.repulsion_magnitude
                    * (.05 + distance / self.characteristic_length) ** -2
                )
                self.forces[block_a]["x"] += np.cos(angle) * repulsion
                self.forces[block_a]["y"] += (
                     np.sin(angle) * repulsion * self.y_factor)

                # Give all the blocks a gentle nudge back toward the
                # center of the plot
                self.forces[block_a]["y"] += (
                    self.zero_y_attraction_magnitude
                    * np.sign(pos_a["y"])
                )
        return None

    def backward_pass(self, arg):
        return self.forces
