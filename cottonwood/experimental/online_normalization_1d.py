"""
This is an implementation of the Online Normalization method from

Online Normalization for Training Neural Networks
Vitaliy Chiley, Ilya Sharapov, Atli Kosson, Urs Koster, Ryan Reece,
Sofía Samaniego de la Fuente, Vishal Subbiah, Michael James
33rd Conference on Neural Information Processing Systems (NeurIPS 2019)
http://papers.nips.cc/paper/9051-online-normalization-for-training-neural-networks.pdf

It's a modified version of the code at
https://github.com/brohrer/online-normalization/blob/master/online-norm/numpy_on/online_norm_1d.py

I made a few significant omissions. These can be added as separate
  layers if you're interested in using them.
* The learned affine transformation (gamma scaling and beta shifting)
  is omitted.
* Activation clamping is omitted. This also is unecessary if you're
  using tanh or other bounded nonlinearity.
* Also there is no longer a difference between training and testing behaviors.
  This means that the moving average for means and variances will
  continue to adapt to testing data.
"""
import numpy as np

EPSILON = 1e-3


class OnlineNormalization1D:
    def __init__(
        self,
        forward_adaptation_rate=0.999,
        backward_adaptation_rate=0.99,
        layer_scaling=False,
    ):
        self.forward_adaptation_rate = forward_adaptation_rate
        self.backward_adaptation_rate = backward_adaptation_rate
        self.layer_scaling = layer_scaling

        self.n_channels = None
        self.running_means = None
        self.running_variances = None
        self.prescaling_error = None
        self.postscaling_error = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "online normalization",
            f"forward adaptation rate: {self.forward_adaptation_rate}",
            f"backward adaptation rate: {self.backward_adaptation_rate}",
            f"layer scaling active?: {self.layer_scaling}",
        ]
        return "\n".join(str_parts)

    def initialize(self):
        self.n_channels = self.forward_in.shape[1]
        self.running_means = np.zeros(self.n_channels)
        self.running_variances = np.ones(self.n_channels)
        self.prescaling_error = np.zeros(self.n_channels)
        self.postscaling_error = np.zeros(self.n_channels)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        channel_means = np.mean(self.forward_in, axis=0, keepdims=False)
        centered_inputs = (
            self.forward_in - channel_means[np.newaxis, :])
        channel_variance = np.mean(
            centered_inputs * centered_inputs,
            axis=0,
            keepdims=False)

        self.running_variances = (
            self.forward_adaptation_rate * self.running_variances +
            (1 - self.forward_adaptation_rate) * channel_variance +
            self.forward_adaptation_rate *
            (1 - self.forward_adaptation_rate) *
            (channel_means - self.running_means) ** 2
        )
        self.running_means += (
            (1 - self.forward_adaptation_rate) *
            (channel_means - self.running_means)
        )

        unscaled_outputs = (
            (self.forward_in - self.running_means) /
            np.sqrt(self.running_variances + EPSILON)
        )
        if self.layer_scaling:
            unscaled_outputs_2nd_moment = np.mean(
                unscaled_outputs * unscaled_outputs,
                axis=0,
                keepdims=True)
            self.layer_scale = np.sqrt(unscaled_outputs_2nd_moment + EPSILON)
            self.forward_out = unscaled_outputs / self.layer_scale
        else:
            self.forward_out = unscaled_outputs

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        unscaled_output_gradient = self.backward_in

        if self.layer_scaling:
            gradient_projection = np.mean(
                unscaled_output_gradient * self.forward_out,
                axis=0,
                keepdims=True) * self.forward_out
            uncorrected_output_gradient = (
                (unscaled_output_gradient - gradient_projection) /
                self.layer_scale)
        else:
            uncorrected_output_gradient = unscaled_output_gradient

        corrected_output_gradient = (
            uncorrected_output_gradient -
            self.prescaling_error[np.newaxis, :] *
            (1 - self.backward_adaptation_rate) *
            self.forward_out
        )
        self.prescaling_error += np.mean(
            corrected_output_gradient * self.forward_out,
            axis=0,
            keepdims=False
        )

        uncorrected_input_gradient = (
            corrected_output_gradient /
            np.sqrt(
                self.running_variances[np.newaxis, :] + EPSILON)
        )
        corrected_input_gradient = (
            uncorrected_input_gradient -
            (1 - self.backward_adaptation_rate) *
            self.postscaling_error[np.newaxis, :]
        )
        self.postscaling_error += np.mean(
            corrected_input_gradient,
            axis=0,
            keepdims=False
        )
        self.backward_out = corrected_input_gradient
        return self.backward_out
