import numpy as np


class PixelNeighborhoodViews:
    """
    Generates a set of views, each centered on a pixel within the
    image and including a small neighborhood around it.
    """
    def __init__(self, neighborhood_size=7):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        self.neighborhood_size = neighborhood_size

    def __str__(self):
        return("Pixel neighborhood views")

    def forward_pass(self, forward_in):
        """
        Generate a set of pixel neighborhood views, expanded from a
        list of inital views.

        forward_in is expected to be either a 3D NumPy array of floats
            or a list of such arrays. If it's a list, the first one is
            the canonical view.

        forward_out will be a list of 3D NumPy arrays.
        """
        self.forward_in = forward_in
        if type(self.forward_in) != list:
            self.forward_in = [self.forward_in]
        n_input_rows, n_input_cols, n_input_channels = self.forward_in[0].shape
        n_output_rows = n_input_rows - self.neighborhood_size + 1
        n_output_cols = n_input_cols - self.neighborhood_size + 1
        output_shape = (n_output_rows, n_output_cols, n_input_channels)

        if np.min(output_shape) <= 0:
            raise ValueError(
                f"Image shape ({n_input_rows}, {n_input_cols}) "
                "too small for translation " +
                f"of {self.neighborhood_size}.")

        # Initialize a list of views, translated versions of the original
        self.forward_out = []
        for source_view in self.forward_in:
            for row_offset in range(n_output_rows):
                for col_offset in range(n_output_cols):
                    neighborhood = source_view[
                        row_offset: row_offset + self.neighborhood_size,
                        col_offset: col_offset + self.neighborhood_size, :]
                    self.forward_out.append(neighborhood)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out
