import numpy as np


class NaiveBayes:
    """
    A Multinomial Naive Bayes model.
    Build a tiny model around each feature and give it a vote.
    Ensemble the lot of them by holding an election.
    """
    def __init__(self, n_categories=2):
        self.n_categories = n_categories
        self.n_features = None
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        self.observations = None
        self.epsilon = 1e-10
        # self.vote_smoothing = 1e-6
        self.probability_floor = 1e-3
        self.observation_baseline = 1
        self.feature_baseline = 3
        self.category_baseline = 0
        # self.category_baseline = 10
        self.last_category = None
        self.last_observation = None

    def __str__(self):
        str_parts = [
            "Multinomial Naive Bayes algorithm",
            f"number of features: {self.n_features}",
            f"number of categories: {self.n_categories}",
        ]
        return "\n".join(str_parts)

    def initialize(self):
        self.n_features = self.observation.size
        self.observations = (
            np.ones((self.n_features, self.n_categories)) *
            self.observation_baseline)
        self.feature_counts = np.ones(self.n_features) * self.feature_baseline
        self.category_counts = (
            np.ones(self.n_categories) * self.category_baseline)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if type(self.forward_in) == tuple:
            self.observation = self.forward_in[0].ravel()
            self.category = int(self.forward_in[1])
        else:
            self.observation = self.forward_in
            self.category = None

        if self.observations is None:
            self.initialize()

        # Estimate the class
        category_votes_by_feature = (
            self.observation[:, np.newaxis] *
            self.observations /
            self.feature_counts[:, np.newaxis])
        category_votes = np.sum(category_votes_by_feature, axis=0)
        e_votes = np.exp(category_votes - np.max(category_votes))
        n_votes = e_votes / (np.sum(e_votes) + self.epsilon)
        self.confidence = -np.log10((1 - np.max(n_votes)) + self.epsilon)
        estimate = int(np.argmax(category_votes))

        '''
        feature_votes = (
            self.observations / self.feature_counts[:, np.newaxis])
        weighted_feature_votes = (
            feature_votes / self.category_counts[np.newaxis, :])
        log_feature_votes = np.log(
            weighted_feature_votes + self.vote_smoothing)
        active_feature_votes = (
            log_feature_votes * self.observation[:, np.newaxis])
        category_votes = np.sum(active_feature_votes, axis=0)
        e_votes = np.exp(category_votes - np.max(category_votes))
        n_votes = e_votes / (np.sum(e_votes) + self.epsilon)
        self.confidence = -np.log10((1 - np.max(n_votes)) + self.epsilon)
        estimate = int(np.argmax(category_votes))
        '''
        '''
        log_probs = np.sum(self.observation[:, np.newaxis] * np.log(
            np.maximum(
                self.probability_floor,
                self.observations / (
                    self.category_counts[np.newaxis, :] + self.epsilon))),
            axis=0)
        estimate = int(np.argmax(log_probs))
        # votes = log_probs - np.min(log_probs)
        # estimate = int(np.argmax(votes))

        # Find the confidence
        # vote_dist = votes / (np.sum(votes) + self.epsilon)
        # self.confidence = vote_dist[estimate]
        self.confidence = np.minimum(1,
            10 * (np.max(log_probs) - np.min(log_probs)) /
            np.sqrt(self.n_features))
        '''

        self.forward_out = estimate

        return self.forward_out

    def add_labeled_observation(self, category):
        # Train the model
        if category is not None:
            self.observations[:, int(category)] += self.observation
            self.feature_counts += self.observation
            self.category_counts[int(category)] += 1
            self.last_category = int(category)
            self.last_observation = self.observation.copy()


    def remove_last_observation(self):
        if self.last_category is not None:
            self.observations[:, self.last_category] += -self.last_observation
            self.feature_counts += -self.last_observation
            self.category_counts[self.last_category] += -1

            self.last_category = None
            self.last_observation = None

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out
