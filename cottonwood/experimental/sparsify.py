"""
For some background on k-sparsity, check out the writeup here:
https://e2eml.school/k_sparse_layer.html
"""
from numba import njit
import numpy as np


class Sparsify:
    """
    Ensure that only a few nodes have a nonzero output.

    This implementation assumes a flattened 1D array
    """
    def __init__(self, fraction=None, n_active_nodes=None):
        self.fraction = fraction
        self.n_active = n_active_nodes
        self.weights = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        self.m_inputs = self.forward_in.size
        self.n_outputs = self.forward_in.size
        if self.n_active is not None:
            self.n_active = int(self.n_active)
        elif self.fraction is not None:
            self.n_active = int(self.m_inputs * self.fraction)
        else:
            self.n_active = int(self.x.size / 2)

        # The weights array is just for show here, but it's
        # really helpful when it comes time to visualize the while network.
        self.weights = np.zeros((self.m_inputs, self.n_outputs))

        # Sensitivity helps rarely active nodes to be more malleable.
        # It helps transform them into something more useful.
        self.s_min = 1
        self.s_max = 10
        self.s_time_const = 100
        self.sensitivity = self.s_min * np.ones(self.m_inputs)

    def __str__(self):
        str_parts = [
            "sparsify",
            f"{self.n_outputs} total nodes",
            f"{self.n_active} active nodes",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.weights is None:
            self.initialize()

        self.weights[np.diag_indices(self.m_inputs)] = 0

        # Find which nodes are active on this pass.
        # They will be the ones with the highest activation.
        i_sort = np.argsort(np.abs(self.forward_in.ravel()))
        self.i_active = i_sort[-self.n_active:]

        # Only propogate the active nodes' activities forward.
        self.weights[self.i_active, self.i_active] = 1
        self.forward_out = np.zeros(self.n_outputs)
        self.forward_out[self.i_active] = self.forward_in[self.i_active]

        # Update the sensitivity for each node.
        # Sensitivity gradually approaches s_max, until a node is active.
        # Then it resets to s_min.
        self.sensitivity += (self.s_max - self.sensitivity) / self.s_time_const
        self.sensitivity[self.i_active] = self.s_min

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        # Only propogate the active nodes' gradients backward.
        self.backward_out = np.zeros(self.m_inputs)
        self.backward_out[self.i_active] = self.backward_in[self.i_active]

        # Ensure that adjustments to nodes that are rarely active
        # will be amplified.
        self.backward_out *= self.sensitivity

        return self.backward_out


class Sparsify2D:
    """
    Ensure that only a few nodes have a nonzero output.

    This implementation assumes a 3D array of shape
    (n_rows, n_cols, n_channels).
    """
    def __init__(self, fraction=None, n_active_nodes=None):
        self.fraction = fraction
        self.input_shape = None
        self.n_active = n_active_nodes

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        self.input_shape = self.forward_in.shape
        if self.n_active is not None:
            self.n_active = int(self.n_active)
        elif self.fraction is not None:
            self.n_active = int(self.forward_in.size * self.fraction)
        else:
            self.n_active = int(self.forward_in.size / 2)

        # Sensitivity helps rarely active nodes to be more malleable.
        # It helps transform them into something more useful.
        self.s_min = 1
        self.s_max = 10
        self.s_time_const = 100
        self.sensitivity = self.s_min * np.ones(self.input_shape)

    def __str__(self):
        str_parts = [
            "sparsify",
            f"{self.input_shape} input dimensions",
            f"{self.n_active} active nodes",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.input_shape is None:
            self.initialize()

        # Find which nodes are active on this pass.
        # They will be the ones with the highest activation.
        x_sorted = np.sort(np.abs(self.forward_in.ravel()))
        cutoff_magnitude = x_sorted[-self.n_active]
        self.i_active = np.where(np.abs(self.forward_in) > cutoff_magnitude)

        # Only propogate the active nodes' activities forward.
        self.forward_out = np.zeros(self.input_shape)
        self.forward_out[self.i_active] = self.forward_in[self.i_active]

        # Update the sensitivity for each node.
        # Sensitivity gradually approaches s_max, until a node is active.
        # Then it resets to s_min.
        self.sensitivity += (self.s_max - self.sensitivity) / self.s_time_const
        self.sensitivity[self.i_active] = self.s_min

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        # Only propogate the active nodes' gradients backward.
        self.backward_out = np.zeros(self.input_shape)
        self.backward_out[self.i_active] = self.backward_in[self.i_active]

        # Ensure that adjustments to nodes that are rarely active
        # will be amplified.
        self.backward_out *= self.sensitivity

        return self.backward_out


class SparsifyByChannel2D:
    """
    Ensure that, for each row and columns,
    only a few channels have a nonzero output.

    This implementation assumes a 3D array of shape
    (n_rows, n_cols, n_channels).
    """
    def __init__(self, fraction=None, n_active_channels=None):
        self.fraction = fraction
        self.input_shape = None
        self.i_active = None
        self.n_active = n_active_channels

        # Sensitivity helps rarely active kernels to be more malleable.
        # It helps transform them into something more useful.
        self.s_min = 1
        self.s_max = 10
        self.s_time_const = 100
        self.sensitivity = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        self.input_shape = self.forward_in.shape
        if self.n_active is None:
            if self.fraction is not None:
                self.n_active = self.input_shape[2] * self.fraction
            else:
                self.n_active = self.input_shape[2] / 2

        # Ensure that the number of active channels is between
        # 1 and the total number of channels.
        self.n_active = int(np.minimum(np.maximum(
            self.n_active, 1), self.input_shape[2]))

        self.sensitivity = self.s_min * np.ones(self.input_shape)

    def __str__(self):
        str_parts = [
            "sparsify channels by location",
            f"{self.input_shape} input dimensions",
            f"{self.n_active} active channels",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.input_shape is None:
            self.initialize()

        # Create an array of indices of values to keep.
        # It's the same size as the inputs, and is all zeros
        # except for the locations that will be kept.
        self.i_inputs = np.zeros(self.input_shape)

        self.i_inputs = sparsify_by_channel(
            self.forward_in, self.i_inputs, self.n_active)
        self.forward_out = self.forward_in * self.i_inputs

        # Update the sensitivity for each node.
        # Sensitivity gradually approaches s_max, until a node is active.
        # Then it resets to s_min.
        self.sensitivity += (self.s_max - self.sensitivity) / self.s_time_const
        self.sensitivity[np.where(self.i_active == 1)] = self.s_min

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        # Only propogate the active nodes' gradients backward.
        self.backward_out = np.zeros(self.input_shape)
        self.backward_out[self.i_active] = self.backward_in[self.i_active]

        # Ensure that adjustments to nodes that are rarely active
        # will be amplified.
        self.backward_out *= self.sensitivity

        return self.backward_out


@njit
def sparsify_by_channel(inputs, i_inputs, n_active):
    n_rows, n_cols, n_channels = inputs.shape
    for i_row in range(n_rows):
        for i_col in range(n_cols):
            # Find which nodes are active in this channel on this pass.
            # They will be the ones with the highest activation.
            channel_magnitudes = np.abs(inputs[i_row, i_col, :])
            channel_sorted = np.sort(channel_magnitudes)
            cutoff_magnitude = channel_sorted[-n_active]
            i_channels = np.where(channel_magnitudes >= cutoff_magnitude)[0]
            for i_channel in i_channels:
                i_inputs[i_row, i_col, i_channel] = 1
    return i_inputs
