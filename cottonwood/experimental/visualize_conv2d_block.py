import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from cottonwood.experimental.visualize_conv2d_kernels import render_kernel
plt.switch_backend("agg")

# These are in centimeters
figure_wid = 16
figure_hgt = 9
border_wid = 1
border_hgt = 1
main_image_wid = 3
main_image_hgt = 3
n_col_factor = 2.2

# All gap, kernel, image, and unit dimensions are multiples of image_wid
gap_wid_unscaled = .15
gap_hgt_unscaled = .25
image_wid_unscaled = 1
image_hgt_unscaled = 1

# Valid Matplotlib colors. More examples here:
# https://e2eml.school/matplotlib_lines.html#color
canvas_color = "#060820"  # very dark blue
activity_cmap = LinearSegmentedColormap.from_list(
    "pos_neg",
    [(0, "red"), (.5, "black"), (1, "white")])
original_2d_cmap = "gray"
border_color = "darkslateblue"
border_color = "#fbf9f6"  # warm white
text_color = "#04253a"
border_linewidth = 1
axes_border_linewidth = .3


def render(
    block,
    original_image=None,
    reports_dir=os.path.join("reports", "convolutions"),
    filename_base=None,
):
    """
    This function is the entry point.
    Show the current state of a two dimensional convolution block.

    block (Conv2D block object)
    original_image (2D or 3D array) is the input image to the neural network
    path (str) is where the image should be stored
    filename_base (str) is the stem of the filename to use
    """
    # Ensure that the necessary directories exist
    os.makedirs(reports_dir, exist_ok=True)

    if filename_base is None:
        filename_base = "conv"
    filename = f"{filename_base}.png"

    for i_kernel in range(block.n_kernels):
        render_kernel(
            block,
            int(i_kernel),
            original_image,
            reports_dir,
            f"{filename_base}_kernel_{i_kernel}.png",
        )

    n_cols = int(np.sqrt(block.n_kernels * n_col_factor))
    n_rows = int(np.ceil(block.n_kernels / n_cols))

    # Image resolution, dots per inch
    dpi = 300

    figure, figure_axes = get_empty_frame()

    # Add the original image
    left = border_wid
    bottom = (figure_hgt - main_image_hgt) / 2
    ax_original_image = add_empty_axes(
        figure, left, bottom, main_image_wid, main_image_hgt)
    if original_image is not None:
        if len(original_image.shape) == 2:
            ax_original_image.imshow(original_image, cmap=original_2d_cmap)
        else:
            ax_original_image.imshow(original_image)

    # How much area is available for individual channels
    channel_region_wid = figure_wid - 3 * border_wid - main_image_wid
    channel_region_hgt = figure_hgt - 2 * border_hgt

    # What would the printed area need to be scaled by
    # if constrained by figure height? width?
    area_wid_unscaled = (
        n_cols * image_wid_unscaled + (n_cols - 1) * gap_wid_unscaled)
    area_hgt_unscaled = (
        n_rows * image_hgt_unscaled + (n_rows - 1) * gap_hgt_unscaled)
    wid_scale = channel_region_wid / area_wid_unscaled
    hgt_scale = channel_region_hgt / area_hgt_unscaled
    # Choose the most constraining dimension
    scale = np.minimum(wid_scale, hgt_scale)

    area_wid = area_wid_unscaled * scale
    area_hgt = area_hgt_unscaled * scale
    image_wid = image_wid_unscaled * scale
    image_hgt = image_hgt_unscaled * scale
    gap_wid = gap_wid_unscaled * scale
    gap_hgt = gap_hgt_unscaled * scale
    area_left = (
        main_image_wid +
        2 * border_wid +
        channel_region_wid / 2 -
        area_wid / 2)
    area_bottom = figure_hgt / 2 - area_hgt / 2

    # Fill in the plots for each channel
    done = False
    i_kernel = 0
    i_col = -1
    while not done:
        i_col += 1
        x_unit = area_left + i_col * (image_wid + gap_wid) + image_wid / 2
        for i_row in range(n_rows):
            y_unit = area_bottom + i_row * (image_hgt + gap_hgt)

            image_ax = add_empty_axes(
                figure,
                x_unit - image_wid / 2,
                y_unit,
                image_wid,
                image_hgt)
            image_ax.imshow(
                np.tanh(block.forward_out[:, :, i_kernel]),
                vmin=-1,
                vmax=1,
                cmap=activity_cmap)

            i_kernel += 1
            if i_kernel >= block.n_kernels:
                done = True  # outer loop
                break  # inner loop

    figure.savefig(os.path.join(reports_dir, filename), dpi=dpi)
    plt.close()


def get_empty_frame():
    # figsize expects inches. Convert from centimeters.
    figure = plt.figure(figsize=(figure_wid / 2.54, figure_hgt / 2.54))

    # Cover the entire figure with an Axes object
    figure_axes = figure.add_axes((0, 0, 1, 1))

    # Ensure that 1 unit in the Axes is 1 centimeter in the final image
    figure_axes.set_xlim(0, figure_wid)
    figure_axes.set_ylim(0, figure_hgt)
    figure_axes.set_facecolor(canvas_color)

    # Clean up the axes
    figure_axes.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    figure_axes.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    figure_axes.spines["top"].set_color(border_color)
    figure_axes.spines["bottom"].set_color(border_color)
    figure_axes.spines["left"].set_color(border_color)
    figure_axes.spines["right"].set_color(border_color)
    figure_axes.spines["top"].set_linewidth(border_linewidth)
    figure_axes.spines["bottom"].set_linewidth(border_linewidth)
    figure_axes.spines["left"].set_linewidth(border_linewidth)
    figure_axes.spines["right"].set_linewidth(border_linewidth)
    return figure, figure_axes


def add_empty_axes(figure, left, bottom, width, height):

    # Cover the entire figure with an Axes object
    axes = figure.add_axes((
        left / figure_wid,
        bottom / figure_hgt,
        width / figure_wid,
        height / figure_hgt))

    # Clean up the axes
    axes.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    axes.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    axes.spines["top"].set_color(border_color)
    axes.spines["bottom"].set_color(border_color)
    axes.spines["left"].set_color(border_color)
    axes.spines["right"].set_color(border_color)
    axes.spines["top"].set_linewidth(axes_border_linewidth)
    axes.spines["bottom"].set_linewidth(axes_border_linewidth)
    axes.spines["left"].set_linewidth(axes_border_linewidth)
    axes.spines["right"].set_linewidth(axes_border_linewidth)

    return axes
