import numpy as np
from numba import njit


class PixelChunks:
    """
    This block takes a 2D array and turns it into a set of features that
    represent square chunks of pixels.
    """
    def __init__(self, chunk_size=2, stride=1):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        self.chunk_size = int(chunk_size)
        self.stride = int(stride)

    def __str__(self):
        return("Pixel chunker")

    def forward_pass(self, forward_in):
        """
        Generate a set of feature values, corresponding to pixel chunks.

        forward_in is expected to be either a 2D NumPy array.

        forward_out will be a 1D NumPy array of chunk values
        """
        self.forward_in = forward_in
        n_rows, n_cols, _ = self.forward_in.shape


        n_chunk_rows = 1 + (n_rows - self.chunk_size) // self.stride
        n_chunk_cols = 1 + (n_cols - self.chunk_size) // self.stride

        chunks = np.zeros(n_chunk_rows * n_chunk_cols)
        chunk_it_up(
            self.forward_in,
            chunks,
            n_chunk_rows,
            n_chunk_cols,
            self.chunk_size,
            self.stride)
        self.forward_out = chunks
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out

@njit
def chunk_it_up(
        pixels,
        chunks,
        n_chunk_rows,
        n_chunk_cols,
        chunk_size,
        stride):
    for i_chunk_row in range(n_chunk_rows):
        for i_chunk_col in range(n_chunk_cols):
            i_chunk = int(i_chunk_row + i_chunk_col * n_chunk_rows)
            chunks[i_chunk] = np.mean(pixels[
                i_chunk_row * stride: i_chunk_row * stride + chunk_size,
                i_chunk_col * stride: i_chunk_col * stride + chunk_size])
