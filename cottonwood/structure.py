import pickle as pkl
import cottonwood.toolbox as tb


class Structure:
    """
    This a foundational data structure. It orchestrates the operations
    of a collection of blocks and their connections.

    The naming convention for connections is that forward flowing signals
    move from the connection's tail to connection's head. The block
    that the forward connection originates from is the "tail block"
    and it terminates on the "head block". This can be confusing because
    the connection originates on the tail block's head port and terminates
    on the head block's tail port. Tail and head is relative to each component.
    They all point the same direction, but when compoenents are placed
    end to end, the head of one is the tail of another.
    """
    def __init__(self):
        self.blocks = {}
        # All connections, indexed by their tail blocks.
        self.connections_by_tail = {}
        # All connections, indexed by their head blocks.
        self.connections_by_head = {}
        self.sorted_blocks = None

    def __str__(self):
        """
        Generate a string summary of all the components of the structure.
        """
        if self.sorted_blocks is None:
            self.find_block_execution_order()

        str_parts = []
        str_parts += self.graph_str()
        for block in self.sorted_blocks:
            str_parts.append(f"{block}:")
            str_parts.append(tb.indent(self.blocks[block].__str__()))
        return "\n".join(str_parts)

    def graph_str(self):
        """
        Return a list of str, compatible with the string assembler in
        the __str__() function.
        """
        connector = " -> "
        nodes = []

        for block in self.sorted_blocks:
            node = block
            head_conns = self.connections_by_tail.get(block)
            if head_conns is not None:
                for head_conn in head_conns.values():
                    nodes.append(node + connector + head_conn["head_block"])
        return nodes

    def add(self, block, block_name):
        """
        Add a block to the collection for use in this structure.
        block_name (str) is the identifier that will be used to access
            the block later.

        Other Structures can be added and treated like blocks.
        This trick can be used to build complex structures out of
        simple ones.
        """
        self.blocks[block_name] = block
        # It's possible that this new block will disrupt the existing
        # execution order. Force a re-calculation.
        self.sorted_blocks = None

    def remove(self, block_name):
        """
        Remove a block from the structure. This also removes all the block's
        connections on both the tail and head side.
        """
        self.remove_head_connections(block_name)
        self.remove_tail_connections(block_name)

        # Finally, remove the block itself
        try:
            del self.blocks[block_name]
        except KeyError:
            print(f"  Block {block_name} not found")

        # Reset the list of sort blocks because now it's all messed up
        self.sorted_blocks = None

    def remove_head_connections(self, block_name):
        """
        Remove the connections that originate at this block's head
        """
        if self.connections_by_tail.get(block_name) is not None:
            # First remove the connections_by_head copy
            for i_port, connection in (
                    self.connections_by_tail.get(block_name).items()):
                head_block = connection["head_block"]
                head_block_port = connection["i_port"]
                try:
                    del self.connections_by_head[head_block][head_block_port]
                except KeyError:
                    print(
                        f"  Connection head {head_block}, {head_block_port}"
                        + " not found")

                # If the head block's only connection has been deleted,
                # Go ahead and remove its entry.
                if len(self.connections_by_head[head_block].keys()) == 0:
                    try:
                        del self.connections_by_head[head_block]
                    except KeyError:
                        print(
                            f"  Connections with head {head_block} not found")

            # Then remove the connections_by_tail copy
            try:
                del self.connections_by_tail[block_name]
            except KeyError:
                print(f"  Connections with tail {block_name} not found")
        self.sorted_blocks = None

    def remove_tail_connections(self, block_name):
        """
        Remove the connections that originate at this block's tail
        """
        if self.connections_by_head.get(block_name) is not None:
            # First remove the connections_by_tail copy
            for i_port, connection in (
                    self.connections_by_head.get(block_name).items()):
                tail_block = connection["tail_block"]
                tail_block_port = connection["i_port"]
                try:
                    del self.connections_by_tail[tail_block][tail_block_port]
                except KeyError:
                    print(
                        f"  Connection head {tail_block}, {tail_block_port}"
                        + " not found")

                # If the tail block's only connection has been deleted,
                # Go ahead and remove its entry.
                if len(self.connections_by_tail[tail_block].keys()) == 0:
                    try:
                        del self.connections_by_tail[tail_block]
                    except KeyError:
                        print(
                            f"  Connections with tail {tail_block} not found")

            # Then remove the connections_by_head copy
            try:
                del self.connections_by_head[block_name]
            except KeyError:
                print(f"  Connections with head {block_name} not found")
        self.sorted_blocks = None

    def connect(
        self,
        tail_block=None,
        head_block=None,
        i_port_tail=0,
        i_port_head=0,
    ):
        """
        head_block and tail_block (str) are the names of the two blocks
            or structures to join. The forward connection will originate
            at the tail block and terminate at the head block.
            The reverse connection will flow in the opposite direction.
        i_port_head and i_port_tail (int) are the port locations
            to connect to in the head block and tail block, respectively.
            i_port_head is the index of the port in the head block and
            i_port_tail is the index of the port in the tail block.
            The part of the nomenclature is particularly confusing.
            I haven't found a better way yet.

        connections_by_tail and connections_by_head contain identical
        information, but they're organized differently. This makes
        it convenient to get at the forward flowing values (fwd_values)
        and backward flowing values (bak_values) from either
        the tail block or the head block.
        """
        if self.blocks.get(head_block) is None:
            raise ValueError(
                f"{head_block} block hasn't been added yet.\n"
                + "You might have made a typo.")
        if self.blocks.get(tail_block) is None:
            raise ValueError(
                f"{tail_block} block hasn't been added yet.\n"
                + "You might have made a typo.")

        if self.connections_by_tail.get(tail_block) is None:
            self.connections_by_tail[tail_block] = {}

        if self.connections_by_head.get(head_block) is None:
            self.connections_by_head[head_block] = {}

        self.connections_by_tail[tail_block][i_port_tail] = {
            "head_block": head_block,
            "i_port": i_port_head,
            "fwd_value": None,
            "bak_value": None,
        }
        self.connections_by_head[head_block][i_port_head] = {
            "tail_block": tail_block,
            "i_port": i_port_tail,
            "fwd_value": None,
            "bak_value": None,
        }
        # It's possible that this new connection disrupted the existing
        # execution order. Force a re-calculation.
        self.sorted_blocks = None

    def connect_sequence(self, blocks):
        """
        A convenience method to connect a sequence of blocks together.

        blocks is a list block names, in the desired order.
        All connections are on head and tail ports 0.
        """
        for i_block in range(len(blocks) - 1):
            self.connect(blocks[i_block], blocks[i_block + 1])

    def forward_pass(self, forward_in=None):
        """
        Complete a forward pass through the structure, iterating through
        self.sorted_blocks.
        """
        # Initialize sorted_blocks if they need it
        if self.sorted_blocks is None:
            self.find_block_execution_order()

        # Repeat the read-compute-write sequence for each block

        for i_block, block in enumerate(self.sorted_blocks):
            if i_block > 0:
                forward_in = self.receive_fwd(block)

            try:
                forward_out = self.blocks[block].forward_pass(forward_in)
            except TypeError:
                print(f"    Execption in the \"{block}\" block.")
                print(
                    f"    You might have added the \"{block}\" block but " +
                    "not connected it up to any other blocks.")
                print(
                    "    Or you might have forgotten" +
                    "to instantiate the class.")
                raise
            except AttributeError:
                print(f"    Execption in the \"{block}\" block.")
                print(
                    f"    You might have added the \"{block}\" block but " +
                    "not connected it up to any other blocks")
                raise
            except Exception:
                print(
                    "    Execption in the forward pass of " +
                    f"\"{block}\" block.")
                raise

            self.send_fwd(block, forward_out)
        return forward_out

    def backward_pass(self, backward_in=None):
        """
        Complete a backward pass through the structure, iterating through
        self.sorted_blocks in reverse.
        """
        # Initialize sorted_blocks if they need it
        if self.sorted_blocks is None:
            self.find_block_execution_order()

        # Repeat the read-compute-write sequence for each block,
        # running in reverse
        for i_block, block in enumerate(self.sorted_blocks[::-1]):
            if i_block > 0:
                backward_in = self.receive_bak(block)
            try:
                # print("running block", block)
                # print("backward_in", backward_in)
                backward_out = self.blocks[block].backward_pass(backward_in)
                # print("backward_out", backward_out)
            except Exception:
                print(
                    "    Execption in the backward pass " +
                    f"of \"{block}\" block.")
                raise

            self.send_bak(block, backward_out)
        return backward_out

    def send_fwd(self, block_name, forward_out):
        """
        Write forward flowing values from a block's head port
        to its connection.
        """
        if self.connections_by_tail.get(block_name) is None:
            return None

        def write_fwd_value(tail_block, i_tail_port, value):
            if self.connections_by_tail[
                    tail_block].get(i_tail_port) is None:
                return

            self.connections_by_tail[
                tail_block][i_tail_port]["fwd_value"] = value
            head_block = self.connections_by_tail[
                tail_block][i_tail_port]["head_block"]
            i_head_port = self.connections_by_tail[
                tail_block][i_tail_port]["i_port"]
            self.connections_by_head[
                head_block][i_head_port]["fwd_value"] = value

        if isinstance(forward_out, tuple):
            for i_tail_port, value in enumerate(forward_out):
                write_fwd_value(block_name, i_tail_port, value)
        else:
            write_fwd_value(block_name, 0, forward_out)

    def send_bak(self, block_name, backward_out):
        """
        Write backward flowing values from a block's tail port
        to its connection.
        """
        if self.connections_by_head.get(block_name) is None:
            return None

        def write_bak_value(head_block, i_head_port, value):
            if self.connections_by_head[
                    head_block].get(i_head_port) is None:
                return

            self.connections_by_head[
                head_block][i_head_port]["bak_value"] = value
            tail_block = self.connections_by_head[
                head_block][i_head_port]["tail_block"]
            i_tail_port = self.connections_by_head[
                head_block][i_head_port]["i_port"]
            self.connections_by_tail[
                tail_block][i_tail_port]["bak_value"] = value

        if isinstance(backward_out, tuple):
            for i_head_port, value in enumerate(backward_out):
                write_bak_value(block_name, i_head_port, value)
        else:
            write_bak_value(block_name, 0, backward_out)

    def receive_fwd(self, block_name):
        """
        Read forward flowing values from a block's connection
        to its tail port .
        """
        if self.connections_by_head.get(block_name) is None:
            return None

        ports_info = self.connections_by_head[block_name].values()
        if len(ports_info) == 0:
            return None
        forward_in = [None] * len(ports_info)
        for i_port, port_info in self.connections_by_head[block_name].items():
            forward_in[i_port] = port_info["fwd_value"]

        if len(ports_info) == 1:
            return forward_in[0]
        else:
            return tuple(forward_in)

    def receive_bak(self, block_name):
        """
        Read backward flowing values from a block's connection
        to its head port.
        """
        if self.connections_by_tail.get(block_name) is None:
            return None

        ports_info = self.connections_by_tail[block_name].values()
        if len(ports_info) == 0:
            return None
        backward_in = [None] * len(ports_info)
        for i_port, port_info in self.connections_by_tail[block_name].items():
            backward_in[i_port] = port_info["bak_value"]

        if len(ports_info) == 1:
            return backward_in[0]
        else:
            return tuple(backward_in)

    def find_block_execution_order(self):
        """
        The blocks aren't assumed to be in the right order at first.
        To make sure they are, re-order them so that all the prerequisite
        blocks have been executed before a new block is executed.

        The hueristic here is
        1. Find all the blocks that have no connections at their tails.
            These are the
            starting points for the computation graph. Add them to a
            to-be-processed queue (TBPQ).
        2. Take the first element from the TBPQ.
        3. Find all the blocks that feed into it.
        4. Check whether each is already in the list of sorted blocks.
        4a. If yes, add the new block to the list of sorted blocks.
            Also, find all the blocks it feeds into. If any of these are
            not in TBPQ, add them.
        4b. If no, append the block back to the end of the TBPQ.
        5. Return to 2. and repeat until TBPQ is empty.
        """
        self.blocks_to_be_sorted = []
        self.sorted_blocks = []
        # These are the blocks that have a connection at their tail.
        # (The connection's head terminates on these blocks.)
        # They are not starting points for the computation.
        non_starting_blocks = self.connections_by_head.keys()
        for block_name in self.blocks.keys():
            if block_name not in non_starting_blocks:
                self.blocks_to_be_sorted.append(block_name)

        while len(self.blocks_to_be_sorted) > 0:
            block = self.blocks_to_be_sorted.pop(0)
            # Check whether this block is ready to be added.
            # It is only ready when all the blocks that feed into it
            # are already in the list of sorted blocks.
            add_this_one = True
            if self.connections_by_head.get(block) is not None:
                feeder_connections = self.connections_by_head[block].values()
                for connection in feeder_connections:
                    feeder_block = connection["tail_block"]
                    if feeder_block not in self.sorted_blocks:
                        add_this_one = False

            # If all the feeder blocks are already accounted for in the
            # list of sorted blocks, then go ahead and add this one.
            if add_this_one:
                self.sorted_blocks.append(block)
                # Add all the blocks that are fed by this block to the TBPQ
                # If they aren't already in it.
                if self.connections_by_tail.get(block) is not None:
                    connections = self.connections_by_tail[block].values()
                    for connection in connections:
                        next_block = connection["head_block"]
                        if next_block not in self.blocks_to_be_sorted:
                            self.blocks_to_be_sorted.insert(0, next_block)
            else:
                # If it's not time to add this block just yet, append
                # it back to the end of the TBPQ so that it can be
                # revisited later.
                self.blocks_to_be_sorted.append(block)


def save_structure(filename, structure):
    with open(filename, "wb") as f:
        pkl.dump(structure, f)


def load_structure(filename):
    with open(filename, "rb") as f:
        structure = pkl.load(f)
    return structure
