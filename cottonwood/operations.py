from copy import deepcopy
import numpy as np


class Constant:
    """
    Generate a single, constant value.
    """
    def __init__(self, constant):
        self.forward_in = None
        self.forward_out = constant
        self.backward_in = None
        self.backward_out = constant

    def __str__(self):
        return "Constant value"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out


class Copy:
    """
    Make a copy of a set of values.

    This is just like a summation but flipped, head-to-tail.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "two copies of inputs"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = (deepcopy(forward_in), deepcopy(forward_in))
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        The backward pass sums the values from both branches.
        """
        self.backward_in = backward_in
        if self.backward_in[0] is None:
            if self.backward_in[1] is None:
                self.backward_out = None
            else:
                self.backward_out = self.backward_in[1]
        elif self.backward_in[1] is None:
            self.backward_out = self.backward_in[0]
        else:
            self.backward_out = (self.backward_in[0] + self.backward_in[1]) / 2

        print(self.backward_out)
        print("backward_out", self.backward_out.shape)
        return self.backward_out


class Difference:
    """
    Subtract one set of values from another.

    Expects a tuple of two identically sized ndarrays.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "difference between inputs"

    def forward_pass(self, forward_in):
        """
        values contains a tuple of two ndarrays of the same shape:
        (subtract_from, to_subtract)

        The result is the difference:
        result = subtract_from - to_subtract
        """
        self.forward_in = forward_in
        self.forward_out = self.forward_in[0] - self.forward_in[1]
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Propagate the gradient dL/dx back to the previous layers.

        Return a tuple of (gradient_of_subtract_from, gradient_of_to_subtract)
        The derivative of subtract_from is 1 and
        the derivative of to_subtract is -1.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
        else:
            self.backward_out = (
                deepcopy(self.backward_in), -deepcopy(self.backward_in))
        return self.backward_out


class Flatten:
    """
    Take in an n-dimensional array and return two dimensional array
    with just one row.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "flatten"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = self.forward_in.ravel()
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
        else:
            self.backward_out = self.backward_in.reshape(self.forward_in.shape)
        return self.backward_out


class HardMax:
    """
    Generates a one dimensional array of zeros of the same size,
    except for a 1 at the location of the maximum value.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "hard max"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = np.zeros(self.forward_in.shape)
        self.forward_out[np.argmax(self.forward_in)] = 1
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class NoBackPass:
    """
    Prevents the backward pass values from being passed to all upstream blocks.
    In backpropagation this blocks the gradient from flowing back
    through the network. It's useful if you want to freeze training on
    upstream layers.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "no gradient past this point"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = self.forward_in
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class NormalSample:
    """
    Generate a single, constant value.
    """
    def __init__(self, mean=0, stddev=1, shape=(1,)):
        self.mean = mean
        self.stddev = stddev
        self.shape = shape
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "Sample from a normal distribution"
            f"mean: {self.mean}",
            f"standard deviation: {self.stddev}",
            f"shape: {self.shape}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_out = np.random.normal(
            loc=self.mean, scale=self.stddev, size=self.shape)
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class OneHot:
    """
    Convert a variable with a set of possible values into an
    array of zeros with just one 1.
    """
    def __init__(self, n_categories):
        self.n_categories = n_categories
        self.categories = {}
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "one hot"

    def get_labels(self):
        """
        Returns an appropriately ordered list of the category labels
        """
        labels = [None] * len(self.categories)
        for label, i_label in self.categories.items():
            labels[i_label] = label
        return labels

    def forward_pass(self, forward_in):
        self.forward_in = forward_in

        label = self.forward_in
        self.forward_out = np.zeros(self.n_categories)
        if label in self.categories.keys():
            self.forward_out[self.categories[label]] = 1
        else:
            n_cats_so_far = len(self.categories.keys())
            if n_cats_so_far < self.n_categories:
                self.categories[label] = n_cats_so_far
            self.forward_out[n_cats_so_far] = 1

        return self.forward_out

    def backward_pass(self, backward_in):
        # Pass through any values that come back this way
        self.backward_in = backward_in
        self.backward_out = self.backward_in
        return self.backward_out


class Stack:
    """
    Concatenate two flattened (one-dimensional) arrays into a single 1D array.
    """
    def __init__(self):
        self.n_rows_0 = None
        self.n_rows_1 = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "stack",
            f"number of rows in port 0: {self.n_rows_0}",
            f"number of rows in port 1: {self.n_rows_1}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in

        arr_0, arr_1 = self.forward_in
        if arr_0 is not None:
            self.n_rows_0 = arr_0.size
        if arr_1 is not None:
            self.n_rows_1 = arr_1.size

        if arr_0 is None:
            if arr_1 is None:
                self.forward_out = None
            else:
                self.forward_out = arr_1
        else:
            if arr_1 is None:
                self.forward_out = arr_0
            else:
                self.forward_out = np.zeros(self.n_rows_0 + self.n_rows_1)
                self.forward_out[:self.n_rows_0] = arr_0
                self.forward_out[self.n_rows_0:] = arr_1
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.n_rows_0 is None:
            if self.n_rows_1 is None:
                self.backward_out = None
            else:
                self.backward_out = self.backward_in
        else:
            if self.n_rows_1 is None:
                self.backward_out = self.backward_in
            else: 
                arr_0 = np.copy(self.backward_in[:self.n_rows_0])
                arr_1 = np.copy(self.backward_in[self.n_rows_0:])
                self.backward_out = (arr_0, arr_1)
        return self.backward_out
