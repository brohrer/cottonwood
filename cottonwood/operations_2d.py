import numpy as np


class Crop:
    """
    Crop an arbitrary number of rows and columns from both sides
    (top, bottom, left, right) of two dimensional data.
    Expects 3D inputs. Operates on the first two dimensions.
    """
    def __init__(self, n_crop=((1, 1), (1, 1))):
        self.n_crop_row_lo = n_crop[0][0]
        self.n_crop_row_hi = n_crop[0][1]
        self.n_crop_col_lo = n_crop[1][0]
        self.n_crop_col_hi = n_crop[1][1]

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "crop 2D",
            f"num crop rows, low side: {self.n_crop_row_lo}",
            f"num crop rows, high side: {self.n_crop_row_hi}",
            f"num crop columns, low side: {self.n_crop_col_lo}",
            f"num crop columns, high side: {self.n_crop_col_hi}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Values is a 3D array
        """
        self.forward_in = forward_in
        if self.forward_in is None:
            self.forward_out = None
            return self.forward_out

        if ((
            self.forward_in.shape[0] <
            self.n_crop_row_lo + self.n_crop_row_hi + 1) or (
            self.forward_in.shape[1] <
            self.n_crop_col_lo + self.n_crop_col_hi + 1
        )):
            raise ValueError(
                f"Array of shape {self.forward_in.shape} " +
                f"too small to crop with these borders")
        self.forward_out = self.forward_in[
            self.n_crop_row_lo: -self.n_crop_row_hi,
            self.n_crop_col_lo: -self.n_crop_col_hi, :]
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Pad some zero values back on to reverse the cropping.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        n_rows, n_cols, n_channels = self.backward_in.shape
        padded = np.zeros((
            n_rows + self.n_crop_row_lo + self.n_crop_row_hi,
            n_cols + self.n_crop_col_lo + self.n_crop_col_hi,
            n_channels))
        padded[
            self.n_crop_row_lo: -self.n_crop_row_hi,
            self.n_crop_col_lo: -self.n_crop_col_hi, :] = (
            self.backward_in)
        self.backward_out = padded
        return self.backward_out


class CropUniform:
    """
    Crop a uniform number of rows and columns from both sides
    (top, bottom, left, right) of two dimensional data.
    Expects 3D inputs. Operates on the first two dimensions.

    This is the inverse of the Pad2D operation.
    """
    def __init__(self, n_crop=1):
        self.n_crop = n_crop
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "crop 2D",
            f"crop width: {self.n_crop}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Values is a 3D array
        """
        self.forward_in = forward_in
        if ((self.forward_in.shape[0] < 2 * self.n_crop + 1) or
                (self.forward_in.shape[1] < 2 * self.n_crop + 1)):
            raise ValueError(
                f"Can't crop array of shape {self.forward_in.shape} " +
                f"by {self.n_crop} rows/columns")
        self.forward_out = self.forward_in[
            self.n_crop: -self.n_crop, self.n_crop: -self.n_crop, :]
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Pad some zero values back on to reverse the cropping.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        n_rows, n_cols, n_channels = self.backward_in.shape
        padded = np.zeros((
            n_rows + 2 * self.n_crop,
            n_cols + 2 * self.n_crop,
            n_channels))
        padded[self.n_crop: -self.n_crop, self.n_crop: -self.n_crop, :] = (
            self.backward_in)
        self.backward_out = padded
        return self.backward_out


class Pad:
    """
    Pad the same number of rows and columns to both sides
    (top, bottom, left, right) of two dimensional data.
    Expects 3D inputs. Operates on the first two dimensions.

    This is the inverse of the Crop2D operation.
    """
    def __init__(self, n_pad=1):
        self.n_pad = n_pad
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "pad 2D",
            f"pad width: {self.n_pad}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Pad some zero values back on to reverse the cropping.
        """
        self.forward_in = forward_in
        n_rows, n_cols, n_channels = self.forward_in.shape
        padded = np.zeros((
            n_rows + 2 * self.n_pad,
            n_cols + 2 * self.n_pad,
            n_channels))
        padded[self.n_pad: -self.n_pad, self.n_pad: -self.n_pad, :] = (
            self.forward_in)
        self.forward_out = padded
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Values is a 3D array
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        if ((self.backward_in.shape[0] < 2 * self.n_pad + 1) or
                (self.backward_in.shape[1] < 2 * self.n_pad + 1)):
            raise ValueError(
                f"Can't crop array of shape {self.backward_in.shape} " +
                f"by {self.n_pad} rows/columns")

        self.backward_out = self.backward_in[
            self.n_pad: -self.n_pad, self.n_pad: -self.n_pad, :]
        return self.backward_out


class Resample:
    """
    Take in a 3D array and return it downsampled or upsampled to
    a specified number of rows and columns.
    """
    def __init__(self, n_rows=None, n_cols=None, scale=1):
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.scale = scale
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "resample to",
            f"number of rows: {self.n_rows}",
            f"number of columns: {self.n_cols}",
        ]
        return "\n".join(str_parts)

    def initialize(self):
        n_rows_in, n_cols_in, _ = self.forward_in.shape
        self.n_rows = int(n_rows_in * self.scale)
        self.n_cols = int(n_cols_in * self.scale)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.n_rows is None:
            self.initialize()

        n_rows_in, n_cols_in, _ = self.forward_in.shape
        i_rows = np.linspace(0, n_rows_in - 1, self.n_rows).astype(int)
        i_cols = np.linspace(0, n_cols_in - 1, self.n_cols).astype(int)
        ii_rows, ii_cols = np.meshgrid(i_rows, i_cols, indexing="ij")
        resampled = np.array(
            forward_in[ii_rows, ii_cols, :], dtype=forward_in.dtype)
        return resampled

    def backward_pass(self, backward_in):
        return self.backward_out


class Stack:
    def __init__(self):
        self.n_rows = None
        self.n_cols = None
        self.n_channels_0 = None
        self.n_channels_1 = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "stack",
            f"number of rows: {self.n_rows}",
            f"number of columns: {self.n_cols}",
            f"number of channels in port 0: {self.n_channels_0}",
            f"number of channels in port 1: {self.n_channels_1}",
        ]
        return "\n".join(str_parts)

    def initialize(self):
        arr_0, arr_1 = self.forward_in
        self.n_rows, self.n_cols, self.n_channels_0 = arr_0.shape
        n_rows_1, n_cols_1, self.n_channels_1 = arr_1.shape
        # Make sure the number of rows and columns are the same in both
        assert self.n_rows == n_rows_1
        assert self.n_cols == n_cols_1

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.n_rows is None:
            self.initialize()

        arr_0, arr_1 = self.forward_in
        self.forward_out = np.zeros((
            self.n_rows, self.n_cols, self.n_channels_0 + self.n_channels_1))
        self.forward_out[:, :, :self.n_channels_0] = arr_0
        self.forward_out[:, :, self.n_channels_0:] = arr_1
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        arr_0 = np.copy(self.backward_in[:, :, :self.n_channels_0])
        arr_1 = np.copy(self.backward_in[:, :, self.n_channels_0:])
        self.backward_out = (arr_0, arr_1)
        return self.backward_out


class SquarePad:
    """
    Pad out a 3D array with zeros
    so that the first two dimensions are the same.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return("pad to square shape")

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        n_rows, n_cols, n_channels = self.forward_in.shape

        padded_size = np.maximum(n_rows, n_cols)
        new_shape = (padded_size, padded_size, n_channels)
        padded_arr = np.zeros(new_shape, dtype=self.foward_in.dtype)
        if n_rows < n_cols:
            offset = int((padded_size - n_rows) / 2)
            padded_arr[:, offset: offset + n_rows, :] = self.forward_in
        else:
            offset = int((padded_size - n_cols) / 2)
            padded_arr[:, :, offset: offset + n_cols] = self.forward_in

        self.forward_out = padded_arr
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class UpsampleDouble:
    """
    Take in a 3D array and return it upsampled to
    double the number of rows and columns (dimensions 0 and 1).
    """
    def __init__(self):
        self.n_input_rows = None
        self.n_input_cols = None
        self.n_output_rows = None
        self.n_output_cols = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "upsample to double size",
            f"number of input rows: {self.n_input_rows}",
            f"number of input columns: {self.n_input_cols}",
            f"number of output rows: {self.n_output_rows}",
            f"number of output columns: {self.n_output_cols}",
        ]
        return "\n".join(str_parts)

    def initialize(self):
        self.n_input_rows, self.n_input_cols, _ = self.forward_in.shape
        self.n_output_rows = self.n_input_rows * 2
        self.n_output_cols = self.n_input_cols * 2

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.n_input_rows is None:
            self.initialize()

        i_rows = np.arange(self.n_input_rows, dtype=int)
        i_cols = np.arange(self.n_input_cols, dtype=int)
        ii_rows, ii_cols = np.meshgrid(i_rows, i_cols, indexing="ij")

        self.forward_out = np.zeros((
            self.n_output_rows, self.n_output_cols, self.forward_in.shape[2]))
        self.forward_out[2 * ii_rows, 2 * ii_cols, :] = self.forward_in
        self.forward_out[2 * ii_rows + 1, 2 * ii_cols, :] = self.forward_in
        self.forward_out[2 * ii_rows, 2 * ii_cols + 1, :] = self.forward_in
        self.forward_out[2 * ii_rows + 1, 2 * ii_cols + 1, :] = self.forward_in
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        i_rows = np.arange(self.n_input_rows, dtype=int)
        i_cols = np.arange(self.n_input_cols, dtype=int)
        ii_rows, ii_cols = np.meshgrid(i_rows, i_cols, indexing="ij")

        self.backward_out = np.zeros((
            self.n_input_rows, self.n_input_cols, self.backward_in.shape[2]))
        self.backward_out += self.backward_in[2 * ii_rows, 2 * ii_cols, :]
        self.backward_out += self.backward_in[2 * ii_rows + 1, 2 * ii_cols, :]
        self.backward_out += self.backward_in[2 * ii_rows, 2 * ii_cols + 1, :]
        self.backward_out += self.backward_in[
            2 * ii_rows + 1, 2 * ii_cols + 1, :]
        return self.backward_out
