from numba import njit
import numpy as np


class AvgPool1D:
    """
    Pool the values, using the average value from each window.
    If the last window doesn't fit completely, just ignore it.

    It operates on a set of one dimensional signals.
    """
    def __init__(self, stride=2, window=3):
        self.stride = stride
        self.window = window
        self.n_channels = None
        self.signal_length = None
        self.pooled_length = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Use the first set of inputs to infer the size of the remaining
        parameters.
        """
        self.n_channels, self.signal_length = self.forward_in.shape
        self.pooled_length = (
            self.signal_length - self.window) // self.stride + 1

    def __str__(self):
        str_parts = [
            "average pooling",
            f"stride: {self.stride}",
            f"window: {self.window}",
            f"number of channels: {self.n_channels}",
            f"signal length: {self.signal_length}",
            f"pooled signal length: {self.pooled_length}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        signals is a two dimensional array
        of shape (n_channels, signal_length).
        Each row is a separate one dimensional signal.
        """
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        self.forward_out = avg_pool_1d(
            self.forward_in, self.window, self.stride, self.pooled_length)
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Transform the gradient with backpropagation and pass it back.
        gradient is a two dimensional array of shape
        (n_channels, gradient_length).
        Each row is the gradient of a separate channel.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = avg_unpool_1d(
            self.backward_in,
            self.window,
            self.stride,
            self.signal_length)
        return self.backward_out


@njit
def avg_pool_1d(signals, window, stride, pooled_length):
    """
    signals is a two dimensional array of shape (signal_length, n_channels).
    window is an integer, the width of the pooling window.
    stride is an integer, the size of the step each time the window shifts.
    pooled_length is the length of each signal after being pooled.

    Returns a two dimensional array of shape (pooled_length, n_channels)
    results contain the average values from each window.
    """
    n_channels = signals.shape[1]
    results = np.zeros((pooled_length, n_channels))
    for i_window in range(pooled_length):
        i_start = i_window * stride
        i_stop = i_window * stride + window
        for i_channel in range(n_channels):
            results[i_window, i_channel] = np.mean(
                signals[i_start:i_stop, i_channel])
    return results


@njit
def avg_unpool_1d(gradient, window, stride, signal_length):
    """
    gradient is a two dimensional array of shape (n_channels, pooled_length).
    window is an integer, the width of the pooling window.
    stride is an integer, the size of the step each time the window shifts.
    signal_length is the length of each signal after being unpooled.

    Returns a two dimensional result of shape (n_channels, signal_length)
    containing the unpooled gradient, ready to be pushed down to the
    previous layer.
    """
    pooled_length, n_channels = gradient.shape
    results = np.zeros((signal_length, n_channels))
    for i_channel in range(n_channels):
        for i_window in range(pooled_length):
            results[
                i_window * stride: i_window * stride + window,
                i_channel
            ] += gradient[i_window, i_channel] / window
    return results


class AvgPool2D:
    """
    Pool the values, using the average value from each window.
    If the last window doesn't fit completely, just ignore it.

    It operates on a set of two dimensional signals.
    It assumes square windows and equal strides in both the row and column
    direction.
    """
    def __init__(self, stride=2, window=2):
        self.stride = stride
        self.window = window
        self.n_channels = None
        self.n_input_rows = None
        self.n_input_cols = None
        self.n_pooled_rows = None
        self.n_pooled_cols = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Use the first set of inputs to infer the size of the remaining
        parameters.
        """
        self.n_input_rows, self.n_input_cols, self.n_channels = \
            self.forward_in.shape
        self.n_pooled_rows = (
            self.n_input_rows - self.window) // self.stride + 1
        self.n_pooled_cols = (
            self.n_input_cols - self.window) // self.stride + 1

    def __str__(self):
        str_parts = [
            "average pooling",
            f"stride: {self.stride}",
            f"window: {self.window}",
            f"number of channels: {self.n_channels}",
            f"input rows: {self.n_input_rows}",
            f"input cols: {self.n_input_cols}",
            f"pooled rows: {self.n_pooled_rows}",
            f"pooled cols: {self.n_pooled_cols}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        inputs is a three dimensional array of shape
        (n_channels, n_input_rows, n_inputs_cols).
        Each dimension zero set of elements contains
        a separate two dimensional signal in dimensions 1 and 2
        """
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        self.forward_out = avg_pool_2d(
            self.forward_in,
            self.window,
            self.stride,
            self.n_pooled_rows,
            self.n_pooled_cols)
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Transform the gradient with backpropagation and pass it back.
        gradient is a two dimensional array of shape
        (n_channels, n_pooled_rows, n_pooled_cols).
        Each row is the gradient of a separate 2D input.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = avg_unpool_2d(
            self.backward_in,
            self.window,
            self.stride,
            self.n_input_rows,
            self.n_input_cols)
        return self.backward_out


@njit
def avg_pool_2d(inputs, window, stride, n_pooled_rows, n_pooled_cols):
    """
    inputs is a two dimensional array of shape
    (n_input_rows, n_input_cols, n_channels).
    window is an integer, the width of the pooling window.
    stride is an integer, the size of the step each time the window shifts.
    n_pooled_rows and n_pooled_cols is the size of each input
    after being pooled.

    Returns a three dimensional array of shape
    (n_pooled_rows, n_pooled_cols, n_channels)
    results contain the average values from each window.
    """
    n_channels = inputs.shape[2]
    results = np.zeros((n_pooled_rows, n_pooled_cols, n_channels))
    for i_window in range(n_pooled_rows):
        i_start = i_window * stride
        i_stop = i_window * stride + window
        for j_window in range(n_pooled_cols):
            j_start = j_window * stride
            j_stop = j_window * stride + window
            for i_channel in range(n_channels):
                results[i_window, j_window, i_channel] = np.mean(
                    inputs[i_start:i_stop, j_start:j_stop, i_channel])
    return results


@njit
def avg_unpool_2d(gradient, window, stride, n_input_rows, n_input_cols):
    """
    gradient is a three dimensional array of shape
    (n_pooled_rows, n_pooled_cols, n_channels).
    window is an integer, the width of the pooling window.
    stride is an integer, the size of the step each time the window shifts.
    n_input_rows, n_input_cols is the size of each input after being unpooled.

    Returns a three dimensional result of shape
    (n_input_rows, n_input_cols, n_channels)
    containing the unpooled gradient, ready to be pushed down to the
    previous layer.
    """
    n_pooled_rows, n_pooled_cols, n_channels = gradient.shape
    results = np.zeros((n_input_rows, n_input_cols, n_channels))
    for i_channel in range(n_channels):
        for i_window in range(n_pooled_rows):
            for j_window in range(n_pooled_cols):
                results[
                    i_window * stride: i_window * stride + window,
                    j_window * stride: j_window * stride + window,
                    i_channel
                ] += gradient[i_window, j_window, i_channel] / window
    return results


class GlobalAvgPool2D:
    """
    Pool the values, using the average value across each channel.
    It operates on a set of two dimensional signals.
    """
    def __init__(self, stride=2, window=2):
        self.n_channels = None
        self.n_input_rows = None
        self.n_input_cols = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Use the first set of inputs to infer the size of the remaining
        parameters.
        """
        self.n_input_rows, self.n_input_cols, self.n_channels = (
            self.foward_in.shape)

    def __str__(self):
        str_parts = [
            "average pooling",
            f"number of channels: {self.n_channels}",
            f"input rows: {self.n_input_rows}",
            f"input cols: {self.n_input_cols}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        inputs is a three dimensional array of shape
        (n_input_rows, n_inputs_cols, n_channels).
        Each dimension zero set of elements contains
        a separate two dimensional signal in dimensions 0 and 1
        """
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        self.forward_out = np.mean(np.mean(self.forward_in, axis=0), axis=0)
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Transform the gradient with backpropagation and pass it back.
        gradient is a two dimensional array of shape
        (n_pooled_rows, n_pooled_cols, n_channels).
        Each panel (dimension 2) is the gradient of a separate 2D input.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = (
            np.reshape(self.backward_in, (-1, 1, 1)) *
            np.ones((self.n_input_rows, self.n_input_cols, self.n_channels)) /
            (self.n_input_rows * self.n_input_cols)
        )
        return self.backward_out


class MaxPool1D:
    """
    Perform pooling, using the maximum value from each window.
    If the last window doesn't fit completely, just ignore it.

    It operates on a set of one dimensional signals.
    """
    def __init__(self, stride=2, window=3):
        self.stride = stride
        self.window = window
        self.n_channels = None
        self.signal_length = None
        self.pooled_length = None
        self.i_max = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Use the first set of inputs to infer the size of the remaining
        parameters.
        """
        self.signal_length, self.n_channels = self.forward_in.shape
        self.pooled_length = (
            self.signal_length - self.window) // self.stride + 1
        self.i_max = np.zeros((self.pooled_length, self.n_channels), dtype=int)

    def __str__(self):
        str_parts = [
            "maximum pooling",
            f"stride: {self.stride}",
            f"window: {self.window}",
            f"number of channels: {self.n_channels}",
            f"signal length: {self.signal_length}",
            f"pooled signal length: {self.pooled_length}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        signals is a two dimensional array
        of shape (n_channels, signal_length).
        Each row is a separate one dimensional signal.
        """
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        self.forward_out = max_pool_1d(
            self.forward_in,
            self.i_max,
            self.window,
            self.stride,
            self.pooled_length)
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Transform the gradient with backpropagation and pass it back.
        gradient is a two dimensional array of shape
        (gradient_length, n_channels).
        Each row is the gradient of a separate channel.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = max_unpool_1d(
            self.backward_in,
            self.i_max,
            self.window,
            self.stride,
            self.signal_length)
        return self.backward_out


@njit
def max_pool_1d(signals, i_max, window, stride, pooled_length):
    """
    signals is a two dimensional array of shape (signal_length, n_channels).
    window is an integer, the width of the pooling window.
    stride is an integer, the size of the step each time the window shifts.
    pooled_length is the length of each signal after being pooled.

    Returns results and modifies i_max
    Both are two dimensional arrays of shape (pooled_length, n_channels)
    results contain the maximum values from each window.
    i_max contains the location within the window
    """
    n_channels = signals.shape[1]
    results = np.zeros((pooled_length, n_channels))
    for i_window in range(pooled_length):
        i_start = i_window * stride
        i_stop = i_window * stride + window
        for i_channel in range(n_channels):
            results[i_window, i_channel] = np.max(
                signals[i_start:i_stop, i_channel])
            i_max[i_window, i_channel] = np.argmax(
                signals[i_start:i_stop, i_channel])
    return results


@njit
def max_unpool_1d(gradient, i_max, window, stride, signal_length):
    """
    gradient and i_max are two dimensional arrays
    of shape (pooled_length, n_channels).
    gradient is what needs to be unpooled, and i_max is the index within
    each window of the maximum value. It's used to assign responsibility
    for the gradient.
    window is an integer, the width of the pooling window.
    stride is an integer, the size of the step each time the window shifts.
    signal_length is the length of each signal after being unpooled.

    Returns a two dimensional result of shape (signal_length, n_channels)
    containing the unpooles gradient, ready to be pushed down to the
    previous layer.
    """
    pooled_length, n_channels = gradient.shape
    results = np.zeros((signal_length, n_channels))
    for i_channel in range(n_channels):
        for i_window in range(pooled_length):
            results[
                i_window * stride + i_max[i_window, i_channel],
                i_channel
            ] = gradient[i_window, i_channel]
    return results


class GlobalMaxPool2D:
    """
    Perform max pooling across the entire 2D image.
    """
    def __init__(self):
        self.stride = None
        self.window = None
        self.n_channels = None
        self.n_input_rows = None
        self.n_input_cols = None
        self.n_pooled_rows = 1
        self.n_pooled_cols = 1
        self.i_row_max = None
        self.i_col_max = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Use the first set of inputs to infer the size of the remaining
        parameters.
        """
        self.n_input_rows, self.n_input_cols, self.n_channels = (
            self.forward_in.shape)
        self.window_rows = self.n_input_rows
        self.window_cols = self.n_input_cols
        self.stride_rows = self.window_rows
        self.stride_cols = self.window_cols
        self.i_row_max = np.zeros(
            (self.n_pooled_rows, self.n_pooled_cols, self.n_channels),
            dtype=int)
        self.i_col_max = np.zeros(
            (self.n_pooled_rows, self.n_pooled_cols, self.n_channels),
            dtype=int)

    def __str__(self):
        str_parts = [
            "global maximum pooling",
            f"number of channels: {self.n_channels}",
            f"number of input rows: {self.n_input_rows}",
            f"number of input columns: {self.n_input_cols}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        inputs is a two dimensional array of shape (n_channels, n_input_rows).
        Each row is a separate one dimensional signal.
        """
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        self.forward_out = max_pool_2d(
            self.forward_in,
            self.i_row_max,
            self.i_col_max,
            self.window_rows,
            self.window_cols,
            self.stride_rows,
            self.stride_cols,
            self.n_pooled_rows,
            self.n_pooled_cols,
        )
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Transform the gradient with backpropagation and pass it back.
        gradient is a three dimensional array of shape
        (n_channels, n_pooled_rows, n_pooled_cols).
        Each step along dimension 0 (row) is the gradient
        of a separate channel.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = max_unpool_2d(
            self.backward_in,
            self.i_row_max,
            self.i_col_max,
            self.window_rows,
            self.window_cols,
            self.stride_rows,
            self.stride_cols,
            self.n_input_rows,
            self.n_input_cols)
        return self.backward_out


class MaxPool2D:
    """
    Perform pooling, using the maximum value from each window.
    If the final window in each row and column
    doesn't fit completely, just ignore it.

    It operates on a set of two dimensional signals.
    """
    def __init__(self, stride=2, window=3):
        self.stride = stride
        self.window = window
        self.n_channels = None
        self.n_input_rows = None
        self.n_input_cols = None
        self.n_pooled_rows = None
        self.n_pooled_cols = None
        self.i_row_max = None
        self.i_col_max = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Use the first set of inputs to infer the size of the remaining
        parameters.
        """
        self.n_input_rows, self.n_input_cols, self.n_channels = (
            self.forward_in.shape)
        self.n_pooled_rows = (
            self.n_input_rows - self.window) // self.stride + 1
        self.n_pooled_cols = (
            self.n_input_cols - self.window) // self.stride + 1
        self.i_row_max = np.zeros(
            (self.n_pooled_rows, self.n_pooled_cols, self.n_channels),
            dtype=int)
        self.i_col_max = np.zeros(
            (self.n_pooled_rows, self.n_pooled_cols, self.n_channels),
            dtype=int)

    def __str__(self):
        str_parts = [
            "maximum pooling",
            f"stride: {self.stride}",
            f"window: {self.window}",
            f"number of channels: {self.n_channels}",
            f"number of input rows: {self.n_input_rows}",
            f"number of input columns: {self.n_input_cols}",
            f"number of pooled rows: {self.n_pooled_rows}",
            f"number of pooled cols: {self.n_pooled_cols}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        inputs is a two dimensional array of shape (n_channels, n_input_rows).
        Each row is a separate one dimensional signal.
        """
        self.forward_in = forward_in
        if self.n_channels is None:
            self.initialize()

        self.forward_out = max_pool_2d(
            self.forward_in,
            self.i_row_max,
            self.i_col_max,
            self.window,
            self.window,
            self.stride,
            self.stride,
            self.n_pooled_rows,
            self.n_pooled_cols,
        )
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Transform the gradient with backpropagation and pass it back.
        gradient is a three dimensional array of shape
        (n_channels, n_pooled_rows, n_pooled_cols).
        Each step along dimension 0 (row) is the gradient
        of a separate channel.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = max_unpool_2d(
            self.backward_in,
            self.i_row_max,
            self.i_col_max,
            self.window,
            self.window,
            self.stride,
            self.stride,
            self.n_input_rows,
            self.n_input_cols)
        return self.backward_out


@njit
def max_pool_2d(
    inputs,
    i_row_max,
    i_col_max,
    window_rows,
    window_cols,
    stride_rows,
    stride_cols,
    n_pooled_rows,
    n_pooled_cols,
):
    """
    inputs is a three dimensional array of shape
    (n_input_rows, n_input_cols, n_channels).
    window_rows and window_cols is an integer,
    the height and width of the pooling window
    in the row and column direction.
    stride_rows and stride_cols are integers,
    the size of the step each time the window shifts.
    n_pooled_rows and n_pooled_cols is the size of each input
    after being pooled.
    After the function runs,
    i_row_max and i_col_max contain the location of the max
    within each window.

    Returns results, a three dimensional array of shape
    (n_pooled_rows, n_pooled_cols, n_channels)
    Contains the maximum values from each window.
    """
    n_channels = inputs.shape[2]
    results = np.zeros((n_pooled_rows, n_pooled_cols, n_channels))
    for i_window_row in range(n_pooled_rows):
        i_start = i_window_row * stride_rows
        i_stop = i_window_row * stride_rows + window_rows
        for j_window_col in range(n_pooled_cols):
            j_start = j_window_col * stride_cols
            j_stop = j_window_col * stride_cols + window_cols
            for i_input in range(n_channels):
                results[i_window_row, j_window_col, i_input] = np.max(
                    inputs[i_start:i_stop, j_start:j_stop, i_input])
                i_max = np.argmax(
                    inputs[i_start:i_stop, j_start:j_stop, i_input])
                # argmax flattens the 2D array first, defaulting to
                # C-style order in which the highest index (column)
                # changes the fastest. That means that
                # i_max floor-div window_cols gives the row
                # and i_max mod window_cols gives the column
                i_row_max[i_window_row, j_window_col, i_input] = (
                    i_max // window_cols)
                i_col_max[i_window_row, j_window_col, i_input] = (
                    i_max % window_cols)
    return results


@njit
def max_unpool_2d(
    gradient,
    i_row_max,
    i_col_max,
    window_rows,
    window_cols,
    stride_rows,
    stride_cols,
    n_input_rows,
    n_input_cols,
):
    """
    gradient, i_row_max, i_col_max are three dimensional arrays
    of shape (n_pooled_rows, n_pooled_cols, n_channels).
    gradient is what needs to be unpooled, and i_max is the index within
    each window of the maximum value. It's used to assign responsibility
    for the gradient.
    window_rows and window_cols are integers,
    the height and width of the pooling window,
    in the row and column direction.
    stride_rows and stride_cols are integers,
    the size of the step each time the window shifts
    in the row and column direction.
    n_input_rows and n_input_cols is the shape of each input channel
    after being unpooled.

    Returns a three dimensional result of shape
    (n_input_rows, n_input_cols, n_channels)
    containing the unpooled gradient, ready to be pushed down to the
    previous layer.
    """
    n_pooled_rows, n_pooled_cols, n_channels = gradient.shape
    results = np.zeros((n_input_rows, n_input_cols, n_channels))
    for i_channel in range(n_channels):
        for i_window_row in range(n_pooled_rows):
            for j_window_col in range(n_pooled_cols):
                results[
                    i_window_row * stride_rows
                    + i_row_max[i_window_row, j_window_col, i_channel],
                    j_window_col * stride_cols
                    + i_col_max[i_window_row, j_window_col, i_channel],
                    i_channel
                ] = gradient[i_window_row, j_window_col, i_channel]
    return results
