import numpy as np


class Dropout:
    def __init__(self, dropout_rate=.2):
        self.dropout_rate = dropout_rate
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        """
        Make a descriptive, human-readable string for this layer.
        """
        str_parts = [
            "dropout",
            f"dropout rate: {self.dropout_rate}"
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Set a random fraction of the inputs to zero
        """
        self.forward_out = forward_in
        self.i_dropout = np.zeros(self.forward_out.size, dtype=bool)
        self.i_dropout[np.where(
            np.random.uniform(size=self.forward_out.size)
            < self.dropout_rate)] = True
        self.forward_out[self.i_dropout] = 0
        self.forward_out[np.logical_not(self.i_dropout)] *= (
            1 / (1 - self.dropout_rate))
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        No changes necessary here. Just pass the gradient on through.
        """
        self.backward_in = backward_in
        self.backward_out = self.backward_in
        return self.backward_out
