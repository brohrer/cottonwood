import numpy as np
from oknn.knn import KNN as OKNN


class KNN:
    """
    An online version of the k-Nearest Neighbors algorithm.
    "Online" here means it handles new data points one at a time
    and incrementally updates the model each time.

    For a description of the k-NN algorithm, check out
    https://youtu.be/KluQCQtHTqk
    """
    def __init__(self, k=5, is_classifier=True, max_node_points=1e10):
        """
        For a detailed description of the arguments, check out the
        code for the OkNN package.
        """
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None
        self.knn = OKNN(
            k=5,
            is_classifier=True,
            max_node_points=max_node_points)

    def __str__(self):
        str_parts = [
            "k-nearest neighbors classifier",
            f"k: {self.knn.k}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        The algorithm handles one example at a time.
        Whatever shape the forward input has, it will be assumed to
        be a set of feature values corresponding to the current
        example and will be flattened.
        """
        self.forward_in = forward_in
        # This is the case where both a feature array and a label are
        # passed in as (feature_array, label)
        if type(forward_in) == tuple:
            self.target = np.array(self.forward_in[0]).ravel()
            self.target_label = self.forward_in[1]
        else:
            # This is the case where only a feature array is passed in.
            self.target = np.array(self.forward_in).ravel()
            self.target_label = None

        estimate = self.knn.update(self.target, self.target_label)
        self.forward_out = estimate
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out
