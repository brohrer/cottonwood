import os
import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib.ticker import FixedLocator, FixedFormatter
plt.switch_backend("agg")

epsilon = 1e-6


class ValueLogger:
    """
    Track a particular value, iteration-by-iteration.
    Save the entire history of the value as a pickled file and
    plot the value history in a png.
    """
    def __init__(
        self,
        dpi=300,
        n_iter_report=1e3,
        log_scale=False,
        report_max=None,
        report_min=None,
        report_name=None,
        reporting_bin_size=None,
        reports_dir="reports",
        value_name="value",
        verbose=True,
    ):
        self.value_history = []
        self.i_iter = 0
        self.log_scale = log_scale
        self.n_iter_report = int(n_iter_report)
        self.report_min = report_min
        self.report_max = report_max
        if reporting_bin_size is not None:
            self.reporting_bin_size = int(
                np.minimum(reporting_bin_size, self.n_iter_report))
        else:
            self.reporting_bin_size = self.n_iter_report
        self.value_name = value_name
        self.verbose = verbose
        self.reports_dir = reports_dir
        if report_name is None:
            self.report_name = f"report_{self.value_name}"
        self.dpi = dpi

        os.makedirs(reports_dir, exist_ok=True)

    def log_value(self, value):
        """
        Grab a copy of the value from each iteration.
        """
        self.value_history.append(value)
        self.i_iter += 1
        if self.verbose:
            if self.i_iter % self.n_iter_report == 0:
                self.report()
                self.write()

    def get_recent_values(self, n_recent_values=None):
        if n_recent_values is None:
            n_recent_values = self.n_iter_report
        if len(self.value_history) > n_recent_values:
            recent_history = np.mean(self.value_history[-n_recent_values:])
        elif len(self.value_history) > 0:
            recent_history = np.mean(self.value_history)
        else:
            recent_history = None
        return recent_history

    def report(self):
        """
        Create a plot of the loss history.
        """
        n_bins = int(len(self.value_history) // self.reporting_bin_size)
        smoothed_history = []
        for i_bin in range(n_bins):
            smoothed_history.append(np.mean(self.value_history[
                i_bin * self.reporting_bin_size:
                (i_bin + 1) * self.reporting_bin_size
            ]))
        if self.log_scale:
            value_history = np.log10(np.array(smoothed_history) + epsilon)
        else:
            value_history = np.array(smoothed_history)

        if self.report_min is None:
            ymin = np.min(value_history) - epsilon
        else:
            ymin = np.minimum(self.report_min, np.min(value_history))
        if self.report_max is None:
            ymax = np.max(value_history) + epsilon
        else:
            ymax = np.maximum(self.report_max, np.max(value_history)) + .1

        fig = plt.figure()
        ax = plt.gca()
        ax.plot(
            np.arange(len(value_history)) + 1,
            value_history,
            color="blue",
        )
        ax.set_xlabel(f"x{self.reporting_bin_size:,} iterations")
        if self.log_scale:
            ax.set_ylabel(f"log10({self.value_name})")
        else:
            ax.set_ylabel(f"{self.value_name}")
        ax.set_ylim(ymin, ymax)
        ax.grid()
        fig.savefig(os.path.join(
            self.reports_dir, self.report_name + ".png"), dpi=self.dpi)
        plt.close()

    def write(self):
        """
        Write the value history to a csv.
        """
        with open(os.path.join(
                self.reports_dir, self.report_name + ".csv"), "w") as f:
            for value in self.value_history:
                f.write(f"{value}\n")


class DistributionLogger:
    """
    Track the distribution of a set of values, iteration-by-iteration.
    Save the entire history of the value as a csv and
    plot the value history in a png.
    """
    def __init__(
        self,
        dpi=300,
        histogram_bin_size=.1,
        n_iter_report=1e4,
        report_name="distribution",
        reports_dir="reports",
        value_max=None,
        value_min=None,
        verbose=True,
    ):
        self.histogram_history = []
        self.values_collector = []
        self.i_iter = 0
        self.n_iter_report = n_iter_report
        self.value_min = value_min
        self.value_max = value_max
        self.histogram_bin_size = histogram_bin_size
        self.report_name = report_name
        self.reports_dir = reports_dir
        self.verbose = verbose
        self.dpi = dpi

        os.makedirs(reports_dir, exist_ok=True)

    def log_values(self, values):
        """
        Grab a copy of the value from each iteration.
        """
        self.values_collector.append(values)
        self.i_iter += 1
        if self.i_iter % self.n_iter_report == 0:
            collected_values = np.array(self.values_collector).ravel()
            # Find the histogram range
            if self.value_min is None:
                value_min = self.histogram_bin_size * np.floor(
                    np.min(collected_values) / self.histogram_bin_size)
            else:
                value_min = self.value_min
            if self.value_max is None:
                value_max = self.histogram_bin_size * np.ceil(
                    np.max(collected_values) / self.histogram_bin_size)
            else:
                value_max = self.value_max
            n_bins = (value_max - value_min) / self.histogram_bin_size

            hist, bin_edges = np.histogram(
                collected_values,
                bins=int(n_bins),
                range=(value_min, value_max))
            self.histogram_history.append((
                hist / self.n_iter_report, bin_edges))
            self.values_collector = []
            if self.verbose:
                self.report()
                self.write()

    def report(self):
        """
        Create a joy plot of the distribution history.
        https://eagereyes.org/blog/2017/joy-plots
        """
        fig = plt.figure()
        ax = plt.gca()

        n_histograms = len(self.histogram_history)
        offset = np.minimum(.5, 3 / n_histograms)
        max_count = 0
        iter_group_label = []
        iter_group_y = []
        for (hist, bin_edges) in self.histogram_history:
            max_count = np.maximum(np.max(hist), max_count)

        for i_hist, (hist, bin_edges) in enumerate(
                self.histogram_history):
            group_y = (n_histograms - i_hist - 1) * offset
            scaled_hist = hist / max_count
            max_count = np.maximum(np.max(hist), max_count)
            x = bin_edges[:-1] + self.histogram_bin_size
            iter_group_label.append(str(i_hist))
            iter_group_y.append(group_y + offset * .5)
            colors = [
                "lightsteelblue",
                "cornflowerblue",
                "royalblue",
                "mediumblue",
                "darkblue",
                "midnightblue",
            ]
            cutoffs = [1, .75, .55, .35, .2, .08]
            for i_level, cutoff in enumerate(cutoffs):
                plt.fill_between(
                    x,
                    y1=group_y + np.minimum(scaled_hist, cutoff),
                    y2=group_y,
                    edgecolor=None,
                    linewidth=.5,
                    facecolor=colors[i_level],
                    zorder=i_hist,
                )
            ax.plot(
                x,
                (n_histograms - i_hist - 1) * offset + scaled_hist,
                color="midnightblue",
                linewidth=.5,
                zorder=i_hist,
            )
        label_spacing = np.maximum(
            1, int(2 ** int(np.log2(len(iter_group_y) / 8))))
        iter_group_y = iter_group_y[label_spacing::label_spacing]
        iter_group_label = iter_group_label[label_spacing::label_spacing]
        y_formatter = FixedFormatter(iter_group_label)
        y_locator = FixedLocator(iter_group_y)
        ax.yaxis.set_major_formatter(y_formatter)
        ax.yaxis.set_major_locator(y_locator)
        ax.tick_params(left=True, right=False, top=True, bottom=True)
        ax.tick_params(axis="x", direction="in")
        ax.tick_params(axis="y", direction="in")
        ax.set_xlabel(f"{self.report_name}")
        ax.set_ylabel(f" x{int(self.n_iter_report):,} iterations")
        ax.set_title(f"Distribution of {self.report_name}")
        if self.value_min is not None and self.value_max is not None:
            ax.set_xlim(self.value_min, self.value_max)
        fig.savefig(os.path.join(
            self.reports_dir, self.report_name + ".png"), dpi=self.dpi)
        plt.close()

    def write(self):
        """
        Write the value history to a csv.
        """
        with open(os.path.join(
                self.reports_dir, self.report_name + ".pkl"), "wb") as f:
            pkl.dump(self.histogram_history, f)


class ConfusionLogger:
    def __init__(
        self,
        n_iter_report=1e4,
        report_name="confusion_matrix",
        reports_dir="reports",
        verbose=True,
    ):
        self.counts = np.zeros((2, 2))
        self.n_cats = 2
        self.label = None
        self.stat_cats = [
            "total_actuals",
            "total_predicted",
            "precision",
            "recall",
        ]
        self.i_iter = 0
        self.n_iter_report = n_iter_report
        self.reports_dir = reports_dir
        if report_name is None:
            self.report_name = f"confusion_matrix"
        else:
            self.report_name = report_name
        self.verbose = verbose

        os.makedirs(reports_dir, exist_ok=True)

    def log_values(self, predicted, actual, labels=None):
        """
        predicted and actual are expected to be one hot category
        representations: 1D arrays, all zeros
        but one element a one, indicating the index of the catgory.
        Labels can be pulled from the OneHot class with OneHot.get_labels()
        """
        self.n_cats = np.maximum(predicted.size, actual.size)
        predicted_mat = np.zeros(self.n_cats)
        predicted_mat[:predicted.size] = predicted.ravel()
        actual_mat = np.zeros(self.n_cats)
        actual_mat[:actual.size] = actual.ravel()
        self.labels = [str(label) for label in labels]

        if self.counts.shape[0] < self.n_cats:
            temp = self.counts
            self.counts = np.zeros((self.n_cats, self.n_cats))
            self.counts[:temp.shape[0], :temp.shape[1]] = temp

        self.counts += predicted_mat[:, np.newaxis] @ actual_mat[np.newaxis, :]

        self.i_iter += 1
        if self.i_iter % self.n_iter_report == 0:
            if self.verbose:
                self.report()
                self.write()

    def calculate_stats(self):
        total_actuals = np.sum(self.counts, axis=0)
        total_predicted = np.sum(self.counts, axis=1)
        precision = np.diag(self.counts) / (total_predicted + 1e-6)
        recall = np.diag(self.counts) / (total_actuals + 1e-6)
        stats = np.concatenate((
            total_actuals[:, np.newaxis],
            total_predicted[:, np.newaxis],
            precision[:, np.newaxis],
            recall[:, np.newaxis]), axis=1)
        return stats

    def calculate_accuracy(self):
        return np.sum(np.diag(self.counts)) / np.sum(self.counts)

    def report(self):
        dpi = 300
        fig = plt.figure(figsize=(16/2.54, 9/2.54))
        ax_main = fig.add_axes((0, 0, 1, 1))
        ax_main.set_xlim((0, 16))
        ax_main.set_ylim((0, 9))
        ax_main.spines["top"].set_visible(False)
        ax_main.spines["right"].set_visible(False)
        ax_main.spines["left"].set_visible(False)
        ax_main.spines["bottom"].set_visible(False)

        conf_left = .15
        conf_bottom = .35
        conf_width = .25
        conf_height = .4
        ax_conf = fig.add_axes((
            conf_left, conf_bottom, conf_width, conf_height))
        ax_conf.set_xlim((0, self.n_cats))
        ax_conf.set_ylim((0, self.n_cats))
        ax_conf.spines["top"].set_visible(False)
        ax_conf.spines["right"].set_visible(False)
        ax_conf.spines["left"].set_visible(False)
        ax_conf.spines["bottom"].set_visible(False)
        ax_conf.tick_params(bottom=False, top=False, left=False, right=False)
        ax_conf.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        n_stat_cats = len(self.stat_cats)
        table_spacing = .12
        table_left = conf_left + conf_width + table_spacing
        table_bottom = conf_bottom
        table_width = 1 - conf_left - table_left
        table_height = conf_height
        ax_table = fig.add_axes((
            table_left, table_bottom, table_width, table_height))
        ax_table.set_xlim((0, n_stat_cats))
        ax_table.set_ylim((0, self.n_cats))
        ax_table.tick_params(bottom=False, top=False, left=False, right=False)
        ax_table.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        cmap = plt.get_cmap("Blues")
        color_values = self.counts / (
            np.sum(self.counts, axis=0) + epsilon)
        # Add patches for each square
        for i_predicted in range(self.n_cats):
            y_predicted = self.n_cats - i_predicted - 1
            for i_actual in range(self.n_cats):
                # Add a rectangular patch around each label
                codes = [Path.MOVETO] + [Path.LINETO] * 3 + [Path.CLOSEPOLY]
                vertices = [
                    (i_actual, y_predicted),
                    (i_actual, y_predicted + 1),
                    (i_actual + 1, y_predicted + 1),
                    (i_actual + 1, y_predicted),
                    (i_actual, y_predicted),
                ]
                vertices = np.array(vertices, float)
                path = Path(vertices, codes)
                color_value = color_values[i_predicted, i_actual]
                facecolor = cmap(color_value)
                if color_value > .5:
                    textcolor = "white"
                else:
                    textcolor = "black"
                pathpatch = PathPatch(
                    path,
                    facecolor=facecolor,
                    edgecolor="black",
                    zorder=0,
                )
                ax_conf.add_patch(pathpatch)

                # Add the label for each of the blocks
                ax_conf.text(
                    i_actual + .5, y_predicted + .5,
                    str(int(self.counts[i_predicted, i_actual])),
                    color=textcolor,
                    fontsize=5,
                    horizontalalignment="center",
                    verticalalignment="center",
                    rotation=0,
                    zorder=6,
                )

        # Add the category labels
        offset = .35
        x_label = self.n_cats + offset
        for i_label, label in enumerate(self.labels):
            y_label = self.n_cats - i_label - .5
            ax_conf.text(
                x_label, y_label,
                label,
                fontsize=6,
                horizontalalignment="left",
                verticalalignment="center",
                rotation=0,
                zorder=6,
            )

        y_label = -offset
        for i_label, label in enumerate(self.labels):
            x_label = i_label + .5
            ax_conf.text(
                x_label, y_label,
                label,
                fontsize=6,
                horizontalalignment="center",
                verticalalignment="top",
                rotation=90,
                zorder=6,
            )
        ax_conf.text(
            -offset, self.n_cats / 2,
            "Predicted label",
            fontsize=8,
            horizontalalignment="right",
            verticalalignment="center",
            rotation=90,
            zorder=6,
        )
        ax_conf.text(
            self.n_cats / 2, self.n_cats + offset,
            "Actual label",
            fontsize=8,
            horizontalalignment="center",
            verticalalignment="bottom",
            rotation=0,
            zorder=6,
        )

        stats = self.calculate_stats()

        for i_stat_label, stat_label in enumerate(self.stat_cats):
            x_label = i_stat_label + .5
            y_label = -offset
            ax_table.text(
                x_label, y_label,
                stat_label.replace("_", "\n"),
                fontsize=6,
                horizontalalignment="center",
                verticalalignment="top",
                rotation=90,
                zorder=6,
            )
            for i_label, label in enumerate(self.labels):
                y_val = self.n_cats - i_label - .5
                if i_stat_label <= 1:
                    ax_table.text(
                        x_label, y_val,
                        str(int(stats[i_label, i_stat_label])),
                        fontsize=6,
                        horizontalalignment="center",
                        verticalalignment="center",
                        rotation=0,
                        zorder=6,
                    )
                else:
                    ax_table.text(
                        x_label, y_val,
                        f"{stats[i_label, i_stat_label]:.3f}",
                        fontsize=6,
                        horizontalalignment="center",
                        verticalalignment="center",
                        rotation=0,
                        zorder=6,
                    )
        fig.savefig(os.path.join(
            self.reports_dir, self.report_name + ".png"), dpi=dpi)
        plt.close()

    def write(self):
        """
        Write the results of the confusion matrix to csvs.
        """
        with open(os.path.join(
                self.reports_dir, self.report_name + ".csv"), "wt") as f:
            f.write(",".join(["pred/actual"] + self.labels) + "\n")
            for i_label, label in enumerate(self.labels):
                f.write(",".join(
                    [label]
                    + list(self.counts[i_label, :].astype(int).astype(str))
                ) + "\n")

        stats_array = self.calculate_stats()
        with open(os.path.join(
                self.reports_dir,
                self.report_name + "_metrics.csv"), "wt") as f:
            f.write(",".join(["category"] + self.stat_cats) + "\n")
            for i_label, label in enumerate(self.labels):
                stat_row = [
                    label,
                    str(int(stats_array[i_label, 0])),
                    str(int(stats_array[i_label, 1])),
                    f"{stats_array[i_label, 2]:.3f}",
                    f"{stats_array[i_label, 3]:.3f}",
                ]
                f.write(",".join(stat_row) + "\n")
