from copy import deepcopy
import numpy as np
from cottonwood.initialization import LSUV
from cottonwood.optimization import Momentum
import cottonwood.toolbox as tb


class Linear:
    """
    inputs are one dimensional arrays of doubles shaped like n_inputs
    If they aren't one dimensional the inputs will be flattened
    and forced into it.
    ouputs are one dimensional arrays of doubles with m_outputs
    n_inputs is inferred from the input block's outputs

    Note that this implementation of a linear layer doesn't include a bias
    element. If you'd like to add that, add a Bias block right after
    this one.

    l1_threshold is the Beta, from the Beta-LASSO method presented in
    Towards Learning Convolutions from Scratch
    Behnam Neyshabur
    https://arxiv.org/abs/2007.13657
    This has been shown to increase sparsity and performance.
    """
    def __init__(
        self,
        n_nodes=None,
        initializer=LSUV(),
        l1_param=None,
        l1_threshold=None,
        l2_param=None,
        optimizer=Momentum(learning_rate=1e-5),
    ):
        self.m_inputs = None
        self.n_outputs = int(n_nodes)
        self.initializer = deepcopy(initializer)
        self.l1_regularization_param = l1_param
        self.l1_regularization_threshold = l1_threshold
        self.l2_regularization_param = l2_param
        self.optimizer = deepcopy(optimizer)
        self.weights = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        self.m_inputs = self.forward_in.size
        # Choose random weights.
        # Inputs match to rows. Outputs match to columns.
        self.weights = self.initializer.initialize(
            self.m_inputs, self.n_outputs)

    def __str__(self):
        """
        Make a descriptive, human-readable string for this block.
        """
        str_parts = [
            "fully connected",
            f"number of inputs: {self.m_inputs}",
            f"number of outputs: {self.n_outputs}",
            f"l1 regularization parameter: {self.l1_regularization_param}",
            f"l1 floor threshold: {self.l1_regularization_threshold}",
            f"l2 regularization parameter: {self.l2_regularization_param}",
            "initialization:" + tb.indent(self.initializer.__str__()),
            "optimizer:" + tb.indent(self.optimizer.__str__()),
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Propagate the inputs forward through the network.
        """
        self.forward_in = forward_in.ravel()[np.newaxis, :]
        x = self.forward_in

        if self.weights is None:
            self.initialize()

        self.forward_out = x @ self.weights
        return self.forward_out.ravel()

    def backward_pass(self, backward_in):
        """
        Propagate the outputs back through the layer.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        dL_dy = self.backward_in.ravel()[np.newaxis, :]
        dy_dw = self.forward_in.transpose()
        dy_dx = self.weights.transpose()

        if self.optimizer.learning_rate > 0:
            self.dL_dw = dy_dw @ dL_dy

            # l1 regularization
            if self.l1_regularization_param is not None:
                self.dL_dw += (
                    np.sign(self.weights) * self.l1_regularization_param)

            # l2 regularization
            if self.l2_regularization_param is not None:
                self.dL_dw += 2 * self.weights * self.l2_regularization_param

            self.optimizer.update(self.weights, self.dL_dw)

            # Beta-LASSO normalization
            if self.l1_regularization_threshold is not None:
                weight_threshold = (
                    self.l1_regularization_threshold *
                    self.optimizer.learning_rate)
                self.weights[np.where(
                    np.abs(self.weights) <= weight_threshold)] = 0

        dL_dx = dL_dy @ dy_dx
        self.backward_out = dL_dx.ravel()
        return self.backward_out
