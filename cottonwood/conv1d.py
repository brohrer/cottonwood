from copy import deepcopy
from numba import njit
import numpy as np
from cottonwood.initialization import LSUV
from cottonwood.optimization import Adam
import cottonwood.toolbox as tb


class Conv1D:
    """
    A one-dimensional (1D) convolutional layer, ready for training with
    backpropagation.

    For a detailed derivation of what this layer does and why:
        https://e2eml.school/convolution_one_d.html

    As input, it expects a two-dimensional (2D) numpy array of floats,
    with shape (n_inputs, n_channels) where
    n_inputs is the length of the 1D signal. This is arbitrary and
        will be specific to the data set.
    n_channels is the number of parallel channels in the inputs.
        For example a time series of 5 different stock prices would be
        a 5-channel input. A set of 128 EEG electrode recordings would
        be 128 channels.

    For output, it will produce a 2D numpy array of floats,
    with shape (n_outputs, n_kernels) where
    n_outputs is the length of the 1D signal after convolution. For now,
        all convolutions are "valid" style, meaning that they are only
        calculated for cases where the kernel overlaps completely with
        the signal. As a result, for a kernel of length k
            n_outputs = n_inputs - k + 1
    n_kernels is the number of separate kernels used. This is an
        arbitrary hyperparameter chosen during the initialization of the layer.

    l1_threshold is the Beta, from the Beta-LASSO method presented in
    Towards Learning Convolutions from Scratch
    Behnam Neyshabur
    https://arxiv.org/abs/2007.13657
    This has been shown to increase sparsity and performance.
    """
    def __init__(
        self,
        initializer=LSUV(),
        kernel_size=3,
        l1_param=None,
        l1_threshold=None,
        l2_param=None,
        n_kernels=5,
        optimizer=Adam(learning_rate=1e-4),
    ):
        # Ensure this is odd
        self.kernel_half = int(kernel_size / 2)
        self.kernel_size = 2 * self.kernel_half + 1

        self.n_channels = None
        self.n_inputs = None
        self.n_kernels = n_kernels
        self.n_outputs = None
        self.weights = None

        self.l1_regularization_param = l1_param
        self.l1_regularization_threshold = l1_threshold
        self.l2_regularization_param = l2_param
        self.initializer = deepcopy(initializer)
        self.optimizer = deepcopy(optimizer)

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Choose random weights for kernel values.

        The initializers expect a 2D array of weights.
        In particular the LSUV initializer will control for the variance
        along each row of the array.

        For CNNs, we would like each convolution result to have
        a variance of about 1, given an input variance of 1. Because the entire
        stack of kernels is added together, we want to treat all the
        kernel values in a stack as a single group when initializing.
        To make sure this happens, we flatten them into a single row.

        After initialization, we need to do some reshaping and swapping
        of dimensions to get the weights into the format we need.
        Dimension 0 ~ kernel values (kernel_size)
        Dimension 1 ~ input channels (n_channels)
        Dimension 2 ~ output channels (n_kernels)
        """
        self.n_inputs, self.n_channels = self.forward_in.shape
        self.n_outputs = self.n_inputs - self.kernel_size + 1

        weights_unshaped = self.initializer.initialize(
            self.kernel_size * self.n_channels, self.n_kernels)
        self.weights = np.reshape(weights_unshaped, (
            self.kernel_size, self.n_channels, self.n_kernels))

    def __str__(self):
        """
        Make a descriptive, human-readable string for this layer.
        """
        str_parts = [
            "convolutional, one dimensional",
            f"number of inputs: {self.n_inputs}",
            f"number of channels: {self.n_channels}",
            f"number of outputs: {self.n_outputs}",
            f"number of kernels: {self.n_kernels}",
            f"kernel size: {self.kernel_size} pixels",
            f"l1 regularization parameter: {self.l1_regularization_param}",
            f"l1 floor threshold: {self.l1_regularization_threshold}",
            f"l2 regularization parameter: {self.l2_regularization_param}",
            "initialization:" + tb.indent(self.initializer.__str__()),
            "weight optimizer:" + tb.indent(self.optimizer.__str__()),
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Propagate the inputs forward through the network.
        """
        self.forward_in = forward_in.copy()
        if self.weights is None:
            self.initialize()

        self.forward_out = calculate_outputs(self.forward_in, self.weights)
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Propagate the outputs back through the layer.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        # Pad the output gradient so that it's prepared to calculate
        # the input and weight gradients.
        # Add the kernel length, less 1, to each end of axis 0.
        dL_dy = np.pad(self.backward_in, (
            (self.weights.shape[0] - 1, self.weights.shape[0] - 1),
            (0, 0)))
        if self.optimizer.learning_rate > 0:
            self.dL_dw = calculate_weight_gradient(self.forward_in, dL_dy)

            # l1 regularization
            if self.l1_regularization_param is not None:
                self.dL_dw += (
                    np.sign(self.weights) * self.l1_regularization_param)

            # l2 regularization
            if self.l2_regularization_param is not None:
                self.dL_dw += 2 * self.weights * self.l2_regularization_param

            self.optimizer.update(self.weights, self.dL_dw)

            # Beta-LASSO normalization
            if self.l1_regularization_threshold is not None:
                weight_threshold = (
                    self.l1_regularization_threshold *
                    self.optimizer.learning_rate)
                self.weights[np.where(
                    np.abs(self.weights) <= weight_threshold)] = 0

        dL_dx = calculate_input_gradient(self.weights, dL_dy)
        self.backward_out = dL_dx
        return self.backward_out


@njit
def calculate_outputs(inputs, kernel_set):
    """
    Compute the multichannel convolutions for a collection of kernels
    and return the assembled result.

    inputs is a 2D array of floats (n_inputs, n_channels) and
    kernel_set is a 3D array of floats (kernel_size, n_channels, n_kernels)

    result will be a 2D array of floats
        (n_inputs - kernel_size + 1, n_kernels)
    """
    n_kernels = kernel_set.shape[2]
    result = np.zeros((inputs.shape[0] - kernel_set.shape[0] + 1, n_kernels))
    for i_kernel in range(n_kernels):
        result[:, i_kernel] = calculate_single_kernel_output(
            inputs, kernel_set[:, :, i_kernel])
    return result


@njit
def calculate_single_kernel_output(inputs, kernel):
    """
    inputs and kernel are 2 dimensional array of floats.
    Each column (dimension 1) represents a separate
    channel. inputs and kernel must have the same number of columns.

    For now, all convolutions are "valid" mode, meaning that they are
    only computed for locations in which the kernel fully overlaps the
    the inputs. This means that the result will be shorter than the
    inputs by the (length of the kernel - 1).

    This seems like a good default behavior since it doesn't involve
    padding. Padding implies fabrication of extra data on the head and
    tail of the inputs which comes with a number of pitfalls and,
    as far as I can see at the moment, not many big advantages.
    """
    result = np.zeros(inputs.shape[0] - kernel.shape[0] + 1)
    for i_channel in range(inputs.shape[1]):
        result += convolve_1d(inputs[:, i_channel], kernel[:, i_channel])
    return result


@njit
def calculate_weight_gradient(inputs, output_grad_padded):
    """
    Compute the partial derivative of the loss function (the overall error)
    with respect to the kernel weights. This is
    a multichannel cross-correlation between output_gradients
    (the partial derivative of the loss with respect to
    the pre-activation function outputs) and the inputs (x).

    kernel_half is (kernel_width - 1) / 2
    inputs is a 2D array of floats shaped as (n_inputs, n_channels)
    output_grad_padded  is a 2D array of floats shaped as
    (n_outputs_pad, n_kernels)
    n_outputs_pad  = n_inputs + kernel_width - 1

    result will be a 3D array of floats shaped as
        (kernel_size, n_channels, n_kernels)
    """
    n_kernels = output_grad_padded.shape[1]
    n_channels = inputs.shape[1]
    kernel_width = output_grad_padded.shape[0] - inputs.shape[0] + 1
    result = np.zeros((kernel_width, n_channels, n_kernels))

    for i_kernel in range(n_kernels):
        result[:, :, i_kernel] = calculate_single_kernel_weight_gradient(
            inputs, output_grad_padded[:, i_kernel])
    return result


@njit
def calculate_single_kernel_weight_gradient(inputs, output_grad_padded):
    """

    inputs is a 2D array of all the layer's inputs shaped like
        (n_inputs, n_channels)
    output_grad_padded is a 1D array of outputs from a single kernel shaped as
        (n_outputs) where n_outputs - n_inputs + 1 is the kernel width

    result is the single kernel weight gradients across all channels,
        shaped like (kernel_width, n_channels)
    """
    n_inputs = inputs.shape[0]
    n_channels = inputs.shape[1]
    n_outputs = output_grad_padded.size
    kernel_width = n_outputs - n_inputs + 1

    result = np.zeros((kernel_width, n_channels))
    for i_channel in range(n_channels):
        result[:, i_channel] = xcorr_1d(
            output_grad_padded, inputs[:, i_channel])
    return result


@njit
def calculate_input_gradient(kernel_set, output_grad_padded):
    """
    Compute the partial derivaticve of the loss function with respect to
    each of the inputs. This is a multichannel cross-correlation
    between output_grad_paddedients
    (the partial derivative of the loss with respect to
    the pre-activation function outputs) and the kernel weights.


    n_inputs  = n_outputs_pad - kernel_width + 1
    kernel_set is a 3D array of floats shaped as
        (kernel_size, n_channels, n_kernels)
    output_grad_padded  is a 2D array of floats shaped as
        (n_outputs_pad, n_kernels)

    result is shaped like inputs, a 2D array of floats
        shaped as (n_inputs, n_channels)
    """
    kernel_size = kernel_set.shape[0]
    n_channels = kernel_set.shape[1]
    n_outputs_pad = output_grad_padded.shape[0]
    n_kernels = output_grad_padded.shape[1]
    n_inputs = n_outputs_pad - kernel_size + 1

    result = np.zeros((n_inputs, n_channels))
    for i_kernel in range(n_kernels):
        result += calculate_single_kernel_input_gradient(
            kernel_set[:, :, i_kernel], output_grad_padded[:, i_kernel])
    return result


@njit
def calculate_single_kernel_input_gradient(kernel, output_grad_padded):
    """
    n_outputs_padded is n_outputs + 2 * (kernel_size - 1)
    kernel is the single kernel weight gradients across all channels,
        shaped like (kernel_size, n_channels)
    output_grad_padded is a 1D array of outputs from a single kernel
        shaped like (n_outputs_padded,)
    n_inputs is n_outputs_padded - kernel_size + 1

    result is a 2D array of all the layer's inputs shaped like
        (n_inputs, n_channels)
    """
    kernel_size = kernel.shape[0]
    n_channels = kernel.shape[1]
    n_outputs_padded = output_grad_padded.size
    n_inputs = n_outputs_padded - kernel_size + 1

    result = np.zeros((n_inputs, n_channels))
    for i_channel in range(n_channels):
        result[:, i_channel] = xcorr_1d(
            output_grad_padded, kernel[:, i_channel])
    return result


@njit
def convolve_1d(inputs, kernel):
    return xcorr_1d(inputs, kernel[::-1])


@njit
def xcorr_1d(inputs, kernel):
    """
    Calculate n_steps of the sliding dot product,
    a.k.a. the cross-correlation,
    between a one dimensional inputs and a one dimensional kernel.

    Start with the beginning (zeroth elements) of the kernel and inputs
    aligned.
    Shift the kernel up by one position each iteration.
    """
    n_steps = inputs.size - kernel.size + 1
    result = np.zeros(n_steps, dtype=np.double)
    for i in range(n_steps):
        result[i] = np.sum(inputs[i: i + kernel.size] * kernel)
    return result


if __name__ == "__main__":
    layer = Conv1D()
    print(layer)
