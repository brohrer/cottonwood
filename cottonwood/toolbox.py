import datetime as dt
import os


def indent(unindented, n_spaces=2):
    """
    Indent a multi-line string using spaces.
    """
    indent = " " * n_spaces
    newline = "\n" + indent
    return indent + newline.join(unindented.split("\n"))


def date_string():
    return dt.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")


def summarize(
    structure,
    reports_dir="reports",
    report_name=None,
):
    """
    Create a human-readable summary of the structure and its parameters.
    """
    if report_name is None:
        report_name = f"structure_summary.txt"
    with open(
        os.path.join(reports_dir, report_name), "w"
    ) as param_file:
        param_file.write(structure.__str__())
