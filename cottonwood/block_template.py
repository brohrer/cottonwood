class BlockTemplate:
    """
    To be a valid block, a class needs to implement these methods.

    They both accept and return a set of values.
    A return tuple is interpreted as multiple arguments,
        each argument corresponding to a different connection.
    A return value of None is used when no value or connection is intended.
    Otherwise, the return value can be any type. ndarrays are a common one,
        but it can be anything. Only tuples are treated specially.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        # String representation for summarization
        return("Template")

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out
