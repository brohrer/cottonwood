import numpy as np
import cottonwood.data.elder_futhark as ef


def get_data_sets():
    """
    This function creates two other functions that generate data.
    One generates a training data set and the other, an evaluation set.

    The examples have the format of a two-dimensional numpy array.
    They can be thought of as a very small (7-pixel by 7-pixel) image.

    The examples are drawn from the 24-rune alphabet of Elder Futhark.


    To use in a script:

        import data_loader_nordic_runes as dat

        training_generator, evaluation_grenerator = dat.get_data_sets()
        new_training_example = next(training_generator)
        new_evaluation_example = next(evaluation_generator)
    """

    examples = list(ef.runes.values())

    def training_set():
        while True:
            index = np.random.choice(len(examples))
            yield examples[index].ravel()

    def evaluation_set():
        while True:
            index = np.random.choice(len(examples))
            yield examples[index].ravel()

    return training_set(), evaluation_set()


class TrainingData:
    def __init__(self):
        self.training_data_generator, _ = get_data_sets()

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "Nordic runes training data"

    def forward_pass(self, forward_in=None):
        self.forward_out = next(self.training_data_generator)
        return self.forward_out

    def backward_pass(self, backward_in=None):
        return self.backward_out


class EvaluationData:
    def __init__(self):
        _, self.evaluation_data_generator = get_data_sets()

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "Nordic runes evaluation data"

    def forward_pass(self, forward_in=None):
        self.forward_out = next(self.evaluation_data_generator)
        return self.forward_out

    def backward_pass(self, backward_in=None):
        return self.backward_out
