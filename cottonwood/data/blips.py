import numpy as np


def get_data_sets():
    """
    This function creates two other functions that generate data.
    One generates a training data set and the other, an evaluation set.

    Each data point is a "blip", a sequence of zeroes with a short,
    nonzero section. Blips come in three varieties, named for the
    approximate shape they take: M, N, V, and H. Each can be inverted as well.

    To use in a script:

        import data_loader_blips as dat

        training_generator, evaluation_grenerator = dat.get_data_sets()
        new_training_example = next(training_generator)
        new_evaluation_example = next(evaluation_generator)
    """

    examples = get_blips()

    def training_set():
        while True:
            i_example = np.random.choice(len(examples))
            yield examples[i_example]

    def evaluation_set():
        while True:
            i_example = np.random.choice(range(len(examples)))
            yield examples[i_example]

    return training_set(), evaluation_set()


def get_blips():
    """
    Blips have four flavors, M, N, V, and H.
    Generate equal numbers of each.
    """
    np.random.seed(87)
    blips = []
    # The length of the signal
    # example_length = 41
    example_length = 21
    # The length of the nonzero section of the signal
    blip_length = 7
    # The total number of examples to generate
    n_examples = 100

    flavors = {
        "M": np.array([1, .7, .4, .1, .4, .7, 1]),
        "V": np.array([-.1, -.4, -.7, -1, -.7, -.4, -.1]),
        "N": np.array([-.7, .7, .4, 0, -.4, -.7, .7]),
        "H": np.array([1, 0, 0, 0, 0, 0, -1]),
    }

    def generate_example(blip):
        example = np.zeros(example_length)
        i_start = np.random.choice(example_length - blip_length - 1)
        example[i_start: i_start + blip_length] = blip
        # Ensure that the example is two dimensional
        # (example_length rows by one channel column)
        return example[:, np.newaxis]

    for _ in range(n_examples):
        # Generate tuples of (example, label)
        # This willl come in handy when it comes time to do classification
        blips.append((generate_example(flavors["M"]), "M"))
        blips.append((generate_example(flavors["V"]), "V"))
        blips.append((generate_example(flavors["N"]), "N"))
        blips.append((generate_example(flavors["H"]), "H"))

    return blips


class TrainingData:
    def __init__(self):
        self.training_data_generator, _ = get_data_sets()

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "blips training data"

    def forward_pass(self, forward_in=None):
        self.forward_out = next(self.training_data_generator)
        return self.forward_out

    def backward_pass(self, backward_in=None):
        return self.backward_out


class EvaluationData:
    def __init__(self):
        _, self.evaluation_data_generator = get_data_sets()

    def __str__(self):
        return "blips evaluation data"

    def forward_pass(self, forward_in=None):
        self.forward_out = next(self.evaluation_data_generator)
        return self.forward_out

    def backward_pass(self, backward_in=None):
        return self.backward_out


if __name__ == "__main__":
    """
    To run a quick test, navigate to the directory containing this module and:
        python3 -m data_loader_blips
    """
    training_block = TrainingData()
    evaluation_block = EvaluationData()
    for _ in range(10):
        new_training_example = training_block.forward_pass()
        print(new_training_example)
    for _ in range(10):
        new_evaluation_example = evaluation_block.forward_pass()
        print(new_evaluation_example)
