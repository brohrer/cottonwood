from copy import deepcopy
from numba import njit
import numpy as np
from cottonwood.initialization import LSUV
from cottonwood.optimization import Adam
import cottonwood.toolbox as tb


class Conv2D:
    """
    A two-dimensional (2D) convolutional layer, ready for training with
    backpropagation.

    As input, it expects a three-dimensional (3D) numpy array of floats,
    with shape (n_rows, n_columns, n_channels) where
    n_rows and n_columns is the number of rows and columns in the inputs.
        This is arbitrary and will be specific to the data set.
        Every image in the dataset needs to have the same number of
        rows and columns, or be pre-processed in order to have a
        consistent number of rows and columns.
    n_channels is the number of parallel channels in the inputs.
        For example an RGB color image would have
        a 3-channel input. A set of 18-band hyperspectral images would
        have 18 channels.

    For output, it will produce a 3D numpy array of floats,
    with shape (n_rows, n_columns, n_kernels) where
    n_rows and n_columns is the size of the 2D inputs after convolution.
        For now, all convolutions are "valid" style, meaning that
        they are only
        calculated for cases where the kernel overlaps completely with
        the input. As a result, for a kernel with k rows and k columns
            n_rows_output = n_rows_input - k + 1
            n_columns_output = n_columns_input - k + 1
    n_kernels is the number of separate kernels used. This is an
        arbitrary hyperparameter chosen during the initialization of the layer.

    l1_threshold is the Beta, from the Beta-LASSO method presented in
    Towards Learning Convolutions from Scratch
    Behnam Neyshabur
    https://arxiv.org/abs/2007.13657
    This has been shown to increase sparsity and performance.
    """
    def __init__(
        self,
        epsilon=1e-2,
        initializer=LSUV(),
        kernel_size=3,
        l1_param=None,
        l1_sparse_param=None,
        l1_threshold=None,
        l2_param=None,
        l2_sparse_param=None,
        linf_param=None,
        n_kernels=5,
        optimizer=Adam(learning_rate=1e-4),
    ):
        # Ensure this is odd.
        # Assume all kernels are square for now, that is,
        # they have the same number of rows and columns.
        kernel_half = int(kernel_size / 2)
        self.kernel_size = 2 * kernel_half + 1
        self.epsilon = epsilon

        self.n_channels = None
        self.n_rows_in = None
        self.n_cols_in = None
        self.n_kernels = n_kernels
        self.n_rows_out = None
        self.n_cols_out = None
        self.dL_dw = None
        self.weights = None

        self.l1_regularization_param = l1_param
        self.l1_sparse_regularization_param = l1_sparse_param
        self.l1_regularization_threshold = l1_threshold
        self.l2_regularization_param = l2_param
        self.l2_sparse_regularization_param = l2_sparse_param
        self.linf_param = linf_param
        self.initializer = deepcopy(initializer)
        self.optimizer = deepcopy(optimizer)

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def initialize(self):
        """
        Choose random weights for kernel values.

        The initializers expect a 2D array of weights.
        In particular the LSUV initializer will control for the variance
        along each row of the array.

        For CNNs, we would like each convolution result to have
        a variance of about 1, given an input variance of 1. Because the entire
        stack of kernels is added together, we want to treat all the
        kernel values in a stack as a single group when initializing.
        To make sure this happens, we flatten them into a single row.

        After initialization, we need to do some reshaping and swapping
        of dimensions to get the weights into the format we need.
        Dimension 0 ~ kernel rows (kernel_size)
        Dimension 1 ~ kernel columns (kernel_size)
        Dimension 2 ~ input channels (n_channels)
        Dimension 3 ~ output channels (n_kernels)
        """
        self.n_rows_in, self.n_cols_in, self.n_channels = self.forward_in.shape

        self.n_rows_out = self.n_rows_in - self.kernel_size + 1
        self.n_cols_out = self.n_cols_in - self.kernel_size + 1

        weights_unshaped = self.initializer.initialize(
            self.kernel_size ** 2 * self.n_channels, self.n_kernels)
        self.weights = np.reshape(weights_unshaped, (
            self.kernel_size,
            self.kernel_size,
            self.n_channels,
            self.n_kernels))

    def __str__(self):
        """
        Make a descriptive, human-readable string for this layer.
        """
        str_parts = [
            "convolutional, one dimensional",
            f"number of input rows: {self.n_rows_in}",
            f"number of input columns: {self.n_cols_in}",
            f"number of channels: {self.n_channels}",
            f"number of output rows: {self.n_rows_out}",
            f"number of output columns: {self.n_cols_out}",
            f"number of kernels: {self.n_kernels}",
            f"kernel size: {self.kernel_size} pixels",
            f"l1 regularization parameter: {self.l1_regularization_param}",
            f"l1 floor threshold: {self.l1_regularization_threshold}",
            f"l2 regularization parameter: {self.l2_regularization_param}",
            "initialization:" + tb.indent(self.initializer.__str__()),
            "optimizer:" + tb.indent(self.optimizer.__str__()),
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Propagate the inputs forward through the network.
        """
        self.forward_in = forward_in.copy()
        if self.weights is None:
            self.initialize()

        self.forward_out = calculate_outputs(self.forward_in, self.weights)
        return self.forward_out

    def backward_pass(self, backward_in):
        """
        Propagate the outputs back through the layer.
        """
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        # Pad the output gradient so that it's prepared to calculate
        # the input and weight gradients.
        # Add the number of kernel rows and columns, less 1, to each end of
        # axis 0 and 1.
        dL_dy = np.pad(self.backward_in, (
            (self.weights.shape[0] - 1, self.weights.shape[0] - 1),
            (self.weights.shape[1] - 1, self.weights.shape[1] - 1),
            (0, 0)))
        if self.optimizer.learning_rate > 0:
            self.dL_dw = calculate_weight_gradient(self.forward_in, dL_dy)

            # l1 regularization
            if self.l1_regularization_param is not None:
                self.dL_dw += (
                    np.sign(self.weights) * self.l1_regularization_param)

            # l1 sparse regularization
            if self.l1_sparse_regularization_param is not None:
                i_weight_updates = np.where(np.abs(self.dL_dw) > self.epsilon)
                self.dL_dw[i_weight_updates] += (
                    np.sign(self.weights[i_weight_updates]) *
                    self.l1_sparse_regularization_param)

            # l2 regularization
            if self.l2_regularization_param is not None:
                self.dL_dw += 2 * self.weights * self.l2_regularization_param

            # l2 sparse regularization
            if self.l2_sparse_regularization_param is not None:
                i_weight_updates = np.where(np.abs(self.dL_dw) > self.epsilon)
                self.dL_dw[i_weight_updates] += (
                    2 * self.weights[i_weight_updates] *
                    self.l2_sparse_regularization_param)

            self.optimizer.update(self.weights, self.dL_dw)

            # Beta-LASSO normalization
            if self.l1_regularization_threshold is not None:
                weight_threshold = (
                    self.l1_regularization_threshold *
                    self.optimizer.learning_rate)
                self.weights[np.where(
                    np.abs(self.weights) <= weight_threshold)] = 0

            if self.linf_param is not None:
                weight_threshold = self.linf_param
                # weight_threshold = self.linf_param / np.sqrt(self.n_channels)
                self.weights[np.where(
                    self.weights > weight_threshold)] = weight_threshold
                self.weights[np.where(
                    self.weights < -weight_threshold)] = -weight_threshold

        dL_dx = calculate_input_gradient(self.weights, dL_dy)
        self.backward_out = dL_dx
        return self.backward_out


class Conv2DFwd(Conv2D):
    """
    This variant of Conv2D doesn't bother with gradients.
    Kernels don't get updated. They remain in their randomly
    initialized states. Upstream blocks don't get valid gradient
    information passed to them.
    """
    def backward_pass(self, backward_in):
        return None


@njit
def calculate_outputs(inputs, kernel_set):
    """
    Compute the multichannel convolutions for a collection of kernels
    and return the assembled result.

    inputs is a 3D array of floats (n_rows_in, n_cols_in, n_channels) and
    kernel_set is a 4D array of floats
    (n_kernel_rows, n_kernel_cols, n_channels, n_kernels)

    result will be a 3D array of floats of shape
        (n_rows_in - n_kernel_rows + 1,
         n_cols_in - n_kernel_cols + 1,
         n_kernels)
    """
    n_kernels = kernel_set.shape[3]
    result = np.zeros((
        inputs.shape[0] - kernel_set.shape[0] + 1,
        inputs.shape[1] - kernel_set.shape[1] + 1,
        n_kernels))
    for i_kernel in range(n_kernels):
        result[:, :, i_kernel] = calculate_single_kernel_output(
            inputs, kernel_set[:, :, :, i_kernel])
    return result


@njit
def calculate_single_kernel_output(inputs, kernel):
    """
    inputs and kernel are 3 dimensional arrays of floats.
    Each panel (dimension 2) represents a separate
    channel. inputs and kernel must have the same size in dimension 2.

    For now, all convolutions are "valid" mode, meaning that they are
    only computed for locations in which the kernel fully overlaps the
    the inputs. This means that the result will be smaller than the
    inputs by the (n_kernel_rows - 1) rows and (n_kernel_cols - 1) cols.

    This seems like a good default behavior since it doesn't involve
    padding. Padding implies fabrication of extra data on the head and
    tail of the inputs which comes with a number of pitfalls and,
    as far as I can see at the moment, not many big advantages.
    """
    result = np.zeros((
        inputs.shape[0] - kernel.shape[0] + 1,
        inputs.shape[1] - kernel.shape[1] + 1
    ))
    for i_channel in range(inputs.shape[2]):
        result += convolve_2d(inputs[:, :, i_channel], kernel[:, :, i_channel])
    return result


@njit
def calculate_weight_gradient(inputs, output_grad_padded):
    """
    Compute the partial derivative of the loss function (the overall error)
    with respect to the kernel weights. This is
    a multichannel cross-correlation between output_gradients
    (the partial derivative of the loss with respect to
    the pre-activation function outputs) and the inputs (x).

    inputs is a 3D array of floats shaped as (n_rows_in, n_cols_in, n_channels)
    output_grad_padded  is a 3D array of floats shape as
    (n_rows_pad_out, n_cols_pad_out, n_kernels)
    n_rows_pad_out  = n_rows_in + n_kernel_rows - 1
    n_cols_pad_out  = n_cols_in + n_kernel_cols - 1

    result will be a 4D array of floats shaped as
        (n_kernel_rows, n_kernel_cols, n_channels, n_kernels)
    """
    n_rows_in = inputs.shape[0]
    n_cols_in = inputs.shape[1]
    n_channels = inputs.shape[2]
    n_rows_pad_out = output_grad_padded.shape[0]
    n_cols_pad_out = output_grad_padded.shape[1]
    n_kernels = output_grad_padded.shape[2]
    n_kernel_rows = n_rows_pad_out - n_rows_in + 1
    n_kernel_cols = n_cols_pad_out - n_cols_in + 1

    result = np.zeros((n_kernel_rows, n_kernel_cols, n_channels, n_kernels))
    for i_kernel in range(n_kernels):
        result[:, :, :, i_kernel] = calculate_single_kernel_weight_gradient(
            inputs, output_grad_padded[:, :, i_kernel])
    return result


@njit
def calculate_single_kernel_weight_gradient(inputs, output_grad_padded):
    """
    inputs is a 3D array of all the block's inputs shaped like
        (n_rows_in, n_cols_in, n_channels)
    output_grad_padded is a 2D array of outputs from a single kernel with
        (n_rows_out, n_cols_out) where
        n_rows_out - n_rows_in + 1 is the number of kernel rows and
        n_cols_out - n_cols_in + 1 is the number of kernel columns

    result is the single kernel weight gradients across all channels,
        shaped like (n_kernel_rows, n_kernel_cols, n_channels)
    """
    n_rows_in = inputs.shape[0]
    n_cols_in = inputs.shape[1]
    n_channels = inputs.shape[2]
    n_rows_out = output_grad_padded.shape[0]
    n_cols_out = output_grad_padded.shape[1]
    n_kernel_rows = n_rows_out - n_rows_in + 1
    n_kernel_cols = n_cols_out - n_cols_in + 1

    result = np.zeros((n_kernel_rows, n_kernel_cols, n_channels))
    for i_channel in range(n_channels):
        result[:, :, i_channel] = xcorr_2d(
            output_grad_padded, inputs[:, :, i_channel])
    return result


@njit
def calculate_input_gradient(kernel_set, output_grad_padded):
    """
    Compute the partial derivative of the loss function with respect to
    each of the inputs. This is a multichannel cross-correlation
    between output_gradients
    (the partial derivative of the loss with respect to
    the pre-activation function outputs) and the kernel weights.

    n_rows_in = n_rows_pad_out - n_kernel_rows + 1
    n_cols_in = n_cols_pad_out - n_kernel_cols + 1
    kernel_set is a 4D array of floats shaped as
        (n_kernel_rows, n_kernel_cols, n_channels, n_kernels)
    output_grad_padded  is a 3D array of floats shape as
        (n_rows_pad_out, n_cols_pad_out, n_kernels)

    result is shaped like inputs, a 3D array of floats
        shaped as (n_rows_in, n_cols_in, n_channels)
    """
    n_kernel_rows = kernel_set.shape[0]
    n_kernel_cols = kernel_set.shape[1]
    n_channels = kernel_set.shape[2]
    n_rows_pad_out = output_grad_padded.shape[0]
    n_cols_pad_out = output_grad_padded.shape[1]
    n_kernels = output_grad_padded.shape[2]
    n_rows_in = n_rows_pad_out - n_kernel_rows + 1
    n_cols_in = n_cols_pad_out - n_kernel_cols + 1

    result = np.zeros((n_rows_in, n_cols_in, n_channels))
    for i_kernel in range(n_kernels):
        result += calculate_single_kernel_input_gradient(
            kernel_set[:, :, :, i_kernel], output_grad_padded[:, :, i_kernel])
    return result


@njit
def calculate_single_kernel_input_gradient(kernel, output_grad_padded):
    """
    n_rows_out_padded is n_rows_out + 2 * (n_kernel_rows - 1)
    n_cols_out_padded is n_cols_out + 2 * (n_kernel_cols - 1)
    kernel is the single kernel weight gradients across all channels,
        shaped like (n_kernel_rows, n_kernel_cols, n_channels)
    output_grad_padded is a 2D array of outputs from a single kernel
        shaped like (n_rows_out_padded, n_cols_out_padded)
    n_rows_in is n_rows_out_padded - n_kernel_rows + 1
    n_cols_in is n_cols_out_padded - n_kernel_cols + 1

    result is a 3D array of all the block's inputs shaped like
        (n_rows_in, n_cols_in, n_channels)
    """
    n_kernel_rows = kernel.shape[0]
    n_kernel_cols = kernel.shape[1]
    n_channels = kernel.shape[2]
    n_rows_out_padded = output_grad_padded.shape[0]
    n_cols_out_padded = output_grad_padded.shape[1]
    n_rows_in = n_rows_out_padded - n_kernel_rows + 1
    n_cols_in = n_cols_out_padded - n_kernel_cols + 1

    result = np.zeros((n_rows_in, n_cols_in, n_channels))
    for i_channel in range(n_channels):
        result[:, :, i_channel] = xcorr_2d(
            output_grad_padded, kernel[:, :, i_channel])
    return result


@njit
def convolve_2d(inputs, kernel):
    return xcorr_2d(inputs, kernel[::-1, ::-1])


@njit
def xcorr_2d(inputs, kernel):
    """
    Calculate n_row_steps and n_col_steps of the sliding dot product,
    a.k.a. the 2D cross-correlation,
    between a two dimensional inputs and a two dimensional kernel.

    Start with the beginning (zeroth) row and column of the kernel and inputs
    aligned.
    """
    n_kernel_rows = kernel.shape[0]
    n_kernel_cols = kernel.shape[1]
    n_row_steps = inputs.shape[0] - n_kernel_rows + 1
    n_col_steps = inputs.shape[1] - n_kernel_cols + 1

    result = np.zeros((n_row_steps, n_col_steps), dtype=np.double)
    for i in range(n_row_steps):
        for j in range(n_col_steps):
            result[i, j] = np.sum(
                inputs[i: i + n_kernel_rows, j: j + n_kernel_cols] * kernel)
    return result


if __name__ == "__main__":
    block = Conv2D()
    print(block)
