from copy import deepcopy
import numpy as np
from cottonwood.optimization import Momentum
import cottonwood.toolbox as tb


class Bias:
    """
    Learn a channel by channel mean offset.
    """
    def __init__(self, optimizer=Momentum(learning_rate=1e-3)):
        self.optimizer = deepcopy(optimizer)
        self.bias = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "additive bias",
            "optimizer:" + tb.indent(self.optimizer.__str__()),
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.bias is None:
            self.bias = np.zeros(self.forward_in.shape)

        self.forward_out = self.forward_in + self.bias
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in

        if self.backward_in is None:
            return None

        dL_db = self.backward_in
        self.optimizer.update(self.bias, dL_db)
        self.backward_out = dL_db
        return self.backward_out


class MinMaxNormalization:
    """
    Transform the values so that they tend to fall between a particular range.
    Default is -.5 to .5

    Renamed from RangeNormalization.
    """
    def __init__(self, norm_min=-.5, norm_max=.5):
        # Estimate the range based on a selection of training data
        self.norm_min = norm_min
        self.norm_max = norm_max
        self.range_min = 1e10
        self.range_max = -1e10

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "range normalization",
            f"range maximum: {self.range_max}",
            f"range minimum: {self.range_min}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        """
        Update the range based on new data.
        """
        self.forward_in = forward_in
        self.range_min = np.minimum(self.range_min, np.min(self.forward_in))
        self.range_max = np.maximum(self.range_max, np.max(self.forward_in))
        self.scale_factor = self.range_max - self.range_min
        self.offset_factor = self.range_min

        # When unscaled, the minimum is 0 and maximum is 1
        unscaled = (self.forward_in - self.offset_factor) / self.scale_factor
        self.forward_out = (
            unscaled * (self.norm_max - self.norm_min) - self.norm_min)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        self.backward_out = self.backward_in / self.scale_factor
        return self.backward_out


class Scale:
    """
    Learn a multiplicative scale factor.
    """
    def __init__(self, optimizer=Momentum(learning_rate=1e-3)):
        self.optimizer = deepcopy(optimizer)
        self.scale = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        str_parts = [
            "multiplicative scaling",
            "optimizer:" + tb.indent(self.optimizer.__str__()),
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.scale is None:
            self.scale = np.ones(self.forward_in.shape)
        self.forward_out = self.forward_in * self.scale
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out is None
            return self.backward_out

        dL_dy = self.backward_in
        dL_dx = dL_dy * self.scale
        dL_ds = dL_dy * self.forward_in
        self.optimizer.update(self.scale, dL_ds)
        self.backward_out = dL_dx
        return self.backward_out
