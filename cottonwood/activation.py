import numpy as np


class Logistic:
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "logistic"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        large_value = 100
        forward_in_chopped = np.minimum(large_value, self.forward_in)
        forward_in_bounded = np.maximum(-large_value, forward_in_chopped)
        self.forward_out = 1 / (1 + np.exp(-forward_in_bounded))
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        d_logistic = self.forward_out * (1 - self.forward_out)
        self.backward_out = self.backward_in * d_logistic
        return self.backward_out


class Sigmoid(Logistic):
    """
    This is the same as the Logistic function.
    """
    def __str__(self):
        return "sigmoid"


class ReLU:
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "rectified linear unit"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = np.maximum(0, self.forward_in)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        d_relu = np.zeros(self.forward_out.shape)
        d_relu[np.where(self.forward_out > 0)] = 1
        self.backward_out = self.backward_in * d_relu
        return self.backward_out


class SoftMax:
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "softmax"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = (
            np.exp(self.forward_in - np.max(self.forward_in)) /
            np.sum(np.exp(self.forward_in - np.max(self.forward_in)))
        )
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        # Ensure that everything is in the right shape
        softmax = np.reshape(self.forward_out, (1, -1))
        grad = np.reshape(self.backward_in, (1, -1))

        d_softmax = (
            softmax * np.identity(softmax.size)
            - softmax.transpose() @ softmax)
        self.backward_out = (grad @ d_softmax).ravel()
        return self.backward_out


class TanH:
    """
    This block applies a hyperbolic tangent nonlinearity to numeric inputs.

    The backward pass updates the gradient for the sake of backpropagation.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "hyperbolic tangent"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.forward_out = np.tanh(self.forward_in)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.backward_in is None:
            self.backward_out = None
            return self.backward_out

        d_tanh = 1 - self.forward_out ** 2
        self.backward_in * d_tanh
        return self.backward_in
