import numpy as np


class AbsoluteLoss:
    """
    This block calculates a loss value as the sum of the absolute value
    of its inputs.
    It also provides a derivative of the loss with respect to each input
    on the backward pass.

    If connected to two input nodes, it will find the sum of the absolute
    values of the difference between the two nodes' values.
    """
    def __init__(self):
        self.loss = None
        self.single_arg = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "absolute error loss"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if type(self.forward_in) == tuple:
            self.single_arg = False
        else:
            self.single_arg = True

        if self.single_arg:
            x1 = self.forward_in
            loss = np.sum(x1)
        else:
            x1, x2 = self.forward_in
            loss = np.sum(np.abs(x1 - x2))

        self.forward_out = loss
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        if self.single_arg:
            self.backward_out = np.sign(self.forward_in)
            return self.backward_out
        else:
            x1, x2 = self.forward_in
            grad = np.sign(x1 - x2)
            self.backward_out = (grad, -grad)
            return self.backward_out


class CrossEntropy:
    """
    Cross entropy is based in information theory and assumes you're
    working with probability distributions. That means that all the
    actual labels are between zero and one and add up to one,
    as do all the predicted labels.

    Cross entropy sounds a bit scary on first exposure, but there are some
    great online tutorials to help get familiar with it.
    https://en.wikipedia.org/wiki/Cross_entropy
    https://rdipietro.github.io/friendly-intro-to-cross-entropy-loss/
    https://adventuresinmachinelearning.com/cross-entropy-kl-divergence/
    https://ml-cheatsheet.readthedocs.io/en/latest/loss_functions.html
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "cross entropy loss"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        predicted_labels, actual_labels = self.forward_in
        loss = - np.sum(actual_labels * np.log(predicted_labels))
        self.forward_out = loss
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        predicted_labels, actual_labels = self.forward_in
        epsilon = 1e-6
        grad = - actual_labels / (predicted_labels + epsilon)
        self.backward_out = (grad, None)
        return self.backward_out


class Hinge:
    """
    Hinge loss is nice because it's simple.
    For multiclass classification problems it reduces to the difference
    between the target class and the highest scoring non-target class.
    It's a good maximum margin classifier, as in support vector machines.

    This implementation assumes a one-hot implementation of the
    distribution for the actual labels, although it won't break badly
    if this isn't true.

    https://en.wikipedia.org/wiki/Hinge_loss
    https://en.wikipedia.org/wiki/Margin_classifier
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None
        self.i_actual = None
        self.i_second = None
        self.size = None

    def __str__(self):
        return "hinge loss"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        predicted_labels = self.forward_in[0].ravel()
        actual_labels = self.forward_in[1].ravel()
        if self.size is None:
            self.size = actual_labels.size

        self.i_actual = np.argmax(actual_labels)
        mod_predicted = np.copy(predicted_labels)
        mod_predicted[self.i_actual] = -1e10
        self.i_second = np.argmax(mod_predicted)
        loss = (
            predicted_labels[self.i_second] - predicted_labels[self.i_actual])
        self.forward_out = loss
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        grad = np.zeros(self.size)
        grad[self.i_actual] = -1
        grad[self.i_second] = 1
        self.backward_out = (grad, -grad)
        return self.backward_out


class SquareLoss:
    """
    This block calculates a loss value as the sum of the square of its inputs.
    It also provides a derivative of the loss with respect to each input
    on the backward pass.

    If connected to two input nodes, it will find the sum of the square
    of the difference ot the two nodes' values.
    """
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        self.single_arg = False

    def __str__(self):
        return "squared error loss"

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if type(self.forward_in) == tuple:
            self.single_arg = False
        else:
            self.single_arg = True

        if self.single_arg:
            loss = np.sum(self.forward_in ** 2)
        else:
            x1, x2 = self.forward_in
            loss = np.sum((x1 - x2) ** 2)
        self.forward_out = loss

        return self.forward_out

    def backward_pass(self, backward_in=None):
        self.backward_in = backward_in
        if self.single_arg:
            grad = 2 * self.forward_in
            self.backward_out = grad
            return self.backward_out

        else:
            x1, x2 = self.forward_in
            grad = 2 * (x1 - x2)
            self.backward_out = (grad, -grad)
            return self.backward_out
